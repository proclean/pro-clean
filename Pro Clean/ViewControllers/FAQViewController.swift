//
//  FAQViewController.swift
//  Pro Clean
//
//  Created by Maged on 5/6/18.
//  Copyright © 2018 Mego. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class FAQViewController: UIViewController {
    var arrRes = ""
    @IBOutlet weak var Space: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
       // print("hi world")
        getdata()
        //print(self.arrRes)
        
       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getdata(){
        let lang = "en"
        let url = "http://softgrow.net/dala/"+lang+"/home/get_page/?page=faq"
        Alamofire.request(url).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                let data = swiftyJsonVar["pageData"]
                //print(data)
                let content = data["content"]
                let main = content.object
                //print(main)
                self.arrRes = main as! String
                //print(self.arrRes)
                self.Space.loadHTMLString(self.arrRes, baseURL: nil)
            }
            
        }
    }
   

}
