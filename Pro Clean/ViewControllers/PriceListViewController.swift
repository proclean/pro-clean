//
//  PriceListViewController.swift
//  Pro Clean
//
//  Created by Nerneen Mohamed on 5/18/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import UIKit
import ReachabilitySwift
class PriceListViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    let contentCellIdentifier = "ContentCellIdentifier"
    let itemCellIdentifier = "ItemCollectionViewCell"
    
    var priceList = [ItemsData]()
    override func viewDidLoad() {
        super.viewDidLoad()
        if(UserDefaults().getDeviceLanguage() == "ar"){
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"side_menu"), style: .plain, target: self, action: #selector(openSideMenu(_:)))
            navigationItem.rightBarButtonItem?.tag = 1
        }else{
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"side_menu"), style: .plain, target: self, action: #selector(openSideMenu(_:)))
            navigationItem.leftBarButtonItem?.tag = 0
        }
        APIUtils.reachabilityNotify(view: self,_selector: #selector(self.reachabilityChanged))
        
        collectionView.register(UINib(nibName: "ContentCollectionViewCell", bundle: nil),
                                forCellWithReuseIdentifier: contentCellIdentifier)
        collectionView.register(UINib(nibName: "ItemCollectionViewCell", bundle: nil),
                                forCellWithReuseIdentifier: itemCellIdentifier)
        navigationItem.title = NSLocalizedString("PriceList", comment: "")
        collectionView.delegate = self
        collectionView.dataSource = self
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
//        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//        layout.itemSize = CGSize(width: 120, height: 120)
//        layout.scrollDirection = .horizontal
        
//        collectionView.collectionViewLayout = layout

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
    }
    @objc public func openSideMenu(_ sender: AnyObject) {
        if(sender.tag == 0){
            self.sideMenuViewController?.presentLeftMenuViewController()
        }else{
            self.sideMenuViewController?.presentRightMenuViewController()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    /// check network reachability
    ///
    /// - Parameter note: notify network change
    @objc func reachabilityChanged(note: Notification) {
        APIUtils.reachabilityChanged(view: self, note: note) { (result) in
            if(result){
                DispatchQueue.main.async {
                    IJProgressView.shared.showProgressView(self.view);
                    
                    let reference = DIDDataHandler()
                    reference.getPriceList(delegate: self)
                }
            }
        }
    }
}
// MARK: - UICollectionViewDataSource
extension PriceListViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return priceList.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // swiftlint:disable force_cast
        if indexPath.section != 0 && indexPath.row == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: itemCellIdentifier,
                                                          for: indexPath) as! ItemCollectionViewCell
           cell.item = self.priceList[indexPath.section - 1]
            if indexPath.section % 2 != 0 {
                cell.backgroundColor = UIColor(white: 242/255.0, alpha: 1.0)
            } else {
                cell.backgroundColor = UIColor.white
            }
            
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: contentCellIdentifier,
                                                          for: indexPath) as! ContentCollectionViewCell
            
            if indexPath.section % 2 != 0 {
                cell.backgroundColor = UIColor(white: 242/255.0, alpha: 1.0)
            } else {
                cell.backgroundColor = UIColor.white
            }
            
            if indexPath.section == 0 {
                if indexPath.row == 0 {
                    cell.contentLabel.text = "Item"
                } else  if indexPath.row == 1{
                    cell.contentLabel.text = "Iron"
                } else if indexPath.row == 2{
                    cell.contentLabel.text = "clean"
                }else if indexPath.row == 3{
                    cell.contentLabel.text = "Iron and clean"
                }else{
                    cell.contentLabel.text = "Section"
                }
                cell.backgroundColor = UIColor.rgb(red: 198.0, green: 236.0, blue: 217.0)
                
            } else {
                if indexPath.row == 1 {
                    cell.contentLabel.text = String(describing: (self.priceList[indexPath.section - 1].price_list?.priceItem!)!)

                } else if indexPath.row == 2{
                    cell.contentLabel.text = String(describing: (self.priceList[indexPath.section - 1].price_list?.priceIron!)!)
                }else if indexPath.row == 3{
                    cell.contentLabel.text = String(describing: (self.priceList[indexPath.section - 1].price_list?.priceIronAndClean!)!)
                }
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.section == 0 {
            if(indexPath.row == 0){
                return CGSize(width: 166, height: 50)
            }else{
                return CGSize(width: 100, height: 50)
            }
        }else{
            if indexPath.row == 0{
                return CGSize(width: 166, height: 30)
            }else{
                return CGSize(width: 100, height: 30)
            }

        }
        
    }
}



extension PriceListViewController :APIDelegate{
    func RequestEndDueToError(error: NSError) {
        IJProgressView.shared.hideProgressView();
        _ = SweetAlert().showAlert(NSLocalizedString("error", comment: ""), subTitle: error.localizedDescription, style: AlertStyle.error)
    }
    func getPriceList(model:[[String:Any]]){
        IJProgressView.shared.hideProgressView();
        var arr = [ItemsData]()
        for item in model{
            arr.append(ItemsData.init(dictionary: item as NSDictionary)!)
        }
        self.priceList = arr
        self.collectionView.reloadData()
    }
    func alertMsg(msg : String){
        IJProgressView.shared.hideProgressView();
        _ = SweetAlert().showAlert(NSLocalizedString("error", comment: ""), subTitle: msg, style: AlertStyle.error)
    }
}
