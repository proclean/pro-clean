//
//  LeftMenuViewController.swift
//  AKSideMenuSimple
//
//  Created by Diogo Autilio on 6/7/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import UIKit

public class LeftMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var tableView: UITableView?
    var arrsideMenuTitles = [NSLocalizedString("mainHome", comment: ""), NSLocalizedString("booking", comment: ""), NSLocalizedString("MyOrders", comment: ""), NSLocalizedString("packages", comment: ""),NSLocalizedString("priceList", comment: ""),NSLocalizedString("setting", comment: ""), NSLocalizedString("faq", comment: ""), NSLocalizedString("userGuide", comment: ""), NSLocalizedString("useOfTerms", comment: ""), NSLocalizedString("shareApp", comment: "")]
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    var images = ["a1", "a2", "a3", "a4", "a5","a6", "IconEmpty", "IconEmpty", "IconEmpty", "IconEmpty"]
    var cellLogin : LoginSideMenuTableViewCell!
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.init(white: 0.15, alpha: 1)
        let tableView = UITableView(frame: CGRect(x: 0, y: 0, width: (self.view.frame.size.width  / 2 ) + 80, height: self.view.frame.size.height ), style: .plain)
        tableView.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleWidth]
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isOpaque = false
        tableView.backgroundColor = .clear
        tableView.backgroundView = nil
        tableView.separatorStyle = .none
        tableView.bounces = false
        let cellNib = UINib(nibName: "LoginSideMenuTableViewCell", bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: "LoginSideMenuTableViewCell")
        tableView.register( UINib(nibName: "SepratorTableViewCell", bundle: nil), forCellReuseIdentifier: "SepratorTableViewCell")
        tableView.register( UINib(nibName: "sideMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "sideMenuTableViewCell")
        tableView.register( UINib(nibName: "titleTableViewCell", bundle: nil), forCellReuseIdentifier: "titleTableViewCell")

        self.tableView = tableView
        self.view.addSubview(self.tableView!)
        NotificationCenter.default.addObserver(self, selector: #selector(updateLoginView(notification:)), name:NSNotification.Name(rawValue: "updateLoginView"), object: nil)
    }
    @objc func updateLoginView(notification: NSNotification ){
        cellLogin.configureCell()
    }
    // MARK: - <UITableViewDelegate>

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            
            if(!UserDefaults().isLoggedIn()){
                let vc = UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "contentLoginVC")
                self.sideMenuViewController!.setContentViewController(vc, animated: true)
                self.sideMenuViewController!.hideMenuViewController()
            }
        case 1:
           let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "contentHomeVC")
           self.sideMenuViewController!.setContentViewController(vc, animated: true)
               self.sideMenuViewController!.hideMenuViewController()
        case 2:
            if(!UserDefaults().isLoggedIn()){
                let vc = UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "contentLoginVC")
                self.sideMenuViewController!.setContentViewController(vc, animated: true)
                self.sideMenuViewController!.hideMenuViewController()
            }else{
                let vc  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BookingViewController")
                self.present(vc, animated: false, completion: nil)
                
            }
            self.sideMenuViewController!.hideMenuViewController()
        case 3:
            if(!UserDefaults().isLoggedIn()){
                let vc = UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "contentLoginVC")
                self.sideMenuViewController!.setContentViewController(vc, animated: true)
                self.sideMenuViewController!.hideMenuViewController()
            }else{
                let vc  = UIStoryboard(name: "Pager", bundle: nil).instantiateViewController(withIdentifier: "contentOrderPager")
                self.sideMenuViewController!.setContentViewController(vc, animated: true)
                self.sideMenuViewController!.hideMenuViewController()
            }
        case 6 :
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "contentSettingVC")
            self.sideMenuViewController!.setContentViewController(vc, animated: true)
            self.sideMenuViewController!.hideMenuViewController()
        case 5:
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "contentPriceListVC")
            self.sideMenuViewController!.setContentViewController(vc, animated: true)
            self.sideMenuViewController!.hideMenuViewController()
        case 4:
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "contentPackagesListVC")
            self.sideMenuViewController!.setContentViewController(vc, animated: true)
            self.sideMenuViewController!.hideMenuViewController()
        case 8:
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "faq")
            self.sideMenuViewController!.setContentViewController(vc, animated: true)
            self.sideMenuViewController!.hideMenuViewController()
        case 10:
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "useofterms")
            self.sideMenuViewController!.setContentViewController(vc, animated: true)
            self.sideMenuViewController!.hideMenuViewController()
            
        case 9 :
            self.sideMenuViewController!.hideMenuViewController()
            let loginController = UserGuideViewController()
            present(loginController, animated: false, completion: nil)
        default:
            break
        }
    }

    // MARK: - <UITableViewDataSource>

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 90
        }else if indexPath.row == 7{
            return 1
        }
        else{
            return 54
        }
    }

    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection sectionIndex: Int) -> Int {
        return arrsideMenuTitles.count + 2
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            cellLogin  = tableView.dequeueReusableCell(withIdentifier: "LoginSideMenuTableViewCell", for: indexPath) as! LoginSideMenuTableViewCell
            cellLogin.configureCell()
            return cellLogin
        }else if(indexPath.row == 7){
            let cell  = tableView.dequeueReusableCell(withIdentifier: "SepratorTableViewCell", for: indexPath) as! SepratorTableViewCell
            return cell
        }else if indexPath.row > 7{
            let cell  = tableView.dequeueReusableCell(withIdentifier: "titleTableViewCell", for: indexPath) as! titleTableViewCell
            cell.titleCell.text = arrsideMenuTitles[indexPath.row - 2]
 
            return cell
        }
        else  {
            let cell  = tableView.dequeueReusableCell(withIdentifier: "sideMenuTableViewCell", for: indexPath) as! sideMenuTableViewCell
            
                            cell.titleSideMenu.text = arrsideMenuTitles[indexPath.row - 1]
                            cell.imgSideMenu.image = UIImage(named: images[indexPath.row - 1])
            
            return cell

        }
    }
}
