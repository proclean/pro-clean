//
//  RightMenuViewController.swift
//  AKSideMenuSimple
//
//  Created by Diogo Autilio on 6/7/16.
//  Copyright © 2016 AnyKey Entertainment. All rights reserved.
//

import UIKit

public class RightMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
//AMColorPickerViewControllerDelegate
    var tableView: UITableView?
    var arrsideMenuTitles = [NSLocalizedString("mainHome", comment: ""), NSLocalizedString("booking", comment: ""), NSLocalizedString("MyOrders", comment: ""), NSLocalizedString("packages", comment: ""),NSLocalizedString("priceList", comment: ""),NSLocalizedString("setting", comment: ""), NSLocalizedString("faq", comment: ""), NSLocalizedString("userGuide", comment: ""), NSLocalizedString("useOfTerms", comment: ""), NSLocalizedString("shareApp", comment: "")]
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.init(white: 0.15, alpha: 1)

        let tableView = UITableView(frame: CGRect(x: 0, y:80, width: (self.view.frame.size.width / 2) + 50, height: self.view.frame.size.height - 90.0), style: .plain)
        

        
        tableView.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleWidth]
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isOpaque = false
        tableView.backgroundColor = .clear
        tableView.backgroundView = nil
        tableView.separatorStyle = .none
        tableView.bounces = false
        tableView.scrollsToTop = false

        self.tableView = tableView
        self.view.addSubview(self.tableView!)
    }

    // MARK: - <UITableViewDelegate>

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "contentHomeVC")
            self.sideMenuViewController!.setContentViewController(vc, animated: true)
            self.sideMenuViewController!.hideMenuViewController()
        case 1:
            let vc = UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "contentLoginVC")
            self.sideMenuViewController!.setContentViewController(vc, animated: true)
            self.sideMenuViewController!.hideMenuViewController()
        case 4:
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "contentPriceListVC")
            self.sideMenuViewController!.setContentViewController(vc, animated: true)
            self.sideMenuViewController!.hideMenuViewController()
        default:
            break
        }
    }
    override public func viewDidAppear(_ animated: Bool) {
        print("viewdid appear")
    }
    override public func viewWillDisappear(_ animated: Bool) {
        print("viewWillDisappear")
    }
    // MARK: - <UITableViewDataSource>

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }

    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection sectionIndex: Int) -> Int {
         return arrsideMenuTitles.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String = "Cell"

        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)

        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
            cell!.backgroundColor = .clear
            cell!.textLabel?.font = UIFont(name: "HelveticaNeue", size: 16)
            cell!.textLabel?.textColor = .black
            cell!.textLabel?.highlightedTextColor = .lightGray
            cell!.selectedBackgroundView = UIView()
        }

        var titles = arrsideMenuTitles
        var images = ["a1", "a2", "a3", "a4", "a5","a6", "a7", "IconEmpty", "IconEmpty", "IconEmpty"]
        cell!.textLabel?.text = titles[indexPath.row]
        cell!.imageView?.image = UIImage(named: images[indexPath.row])
        
//        cell!.textLabel?.text = titles[indexPath.row]
        cell!.textLabel?.textAlignment = .right

        return cell!
    }
}
