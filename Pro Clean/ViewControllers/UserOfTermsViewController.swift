//
//  UserOfTermsViewController.swift
//  Pro Clean
//
//  Created by Maged on 5/6/18.
//  Copyright © 2018 Mego. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class UserOfTermsViewController: UIViewController {
   @IBOutlet weak var MySpace: UIWebView!
    
    var arrRes = "" as String
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getdata()
      

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getdata(){
        Alamofire.request("http://softgrow.net/dala/en/home/get_page/?page=terms").responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                let data = swiftyJsonVar["pageData"]
                let content = data["content"]
                let main = (content.object) as! String
                //print(main)
                self.arrRes = main
                //print(self.arrRes)
                 self.MySpace.loadHTMLString(self.arrRes, baseURL: nil)
            }
            
        }
        
        //print(arrRes)
       
    }
 

}
