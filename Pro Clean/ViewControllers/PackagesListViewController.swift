//
//  PackagesListViewController.swift
//  Pro Clean
//
//  Created by Nerneen Mohamed on 5/18/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import UIKit
import ReachabilitySwift
class PackagesListViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var packageList = [PackagesData]()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"back_icon"), style: .plain, target: self, action: #selector(doBack(_:)))
        navigationItem.title = NSLocalizedString("Packages", comment: "")
        APIUtils.reachabilityNotify(view: self,_selector: #selector(self.reachabilityChanged))
        collectionView.backgroundColor = UIColor.init(white: 0.95, alpha: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @objc public func doBack(_ sender: AnyObject) {
        doClear()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : RootViewController = storyboard.instantiateViewController(withIdentifier: "rootController") as! RootViewController
        self.present(vc, animated: false, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
    }
    func doClear(){
        for item in self.view.subviews{
            item.removeFromSuperview()
        }
        if(collectionView != nil){
            collectionView.removeFromSuperview()
        }
    }
    deinit {
        for item in self.view.subviews{
            item.removeFromSuperview()
        }
    }
    /// check network reachability
    ///
    /// - Parameter note: notify network change
    @objc func reachabilityChanged(note: Notification) {
        APIUtils.reachabilityChanged(view: self, note: note) { (result) in
            if(result){
                DispatchQueue.main.async {
                    IJProgressView.shared.showProgressView(self.view);

                    let reference = DIDDataHandler()
                    reference.getPackageList(delegate: self)
                }
            }
        }
    }
}
extension PackagesListViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return packageList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PackageItemCell", for: indexPath) as! PackageItemCell
        cell.isSelected = false
        cell.model = packageList[indexPath.row]
      //  cell.configureCell(model: self.packages[indexPath.row])
        Common.instance.CornerView(cell)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width - 20, height: 255)
    }
}
extension PackagesListViewController :APIDelegate{
    func RequestEndDueToError(error: NSError) {
        IJProgressView.shared.hideProgressView();
        _ = SweetAlert().showAlert(NSLocalizedString("error", comment: ""), subTitle: error.localizedDescription, style: AlertStyle.error)
    }
    func packages(model:[[String:Any]]){
        IJProgressView.shared.hideProgressView();
        var arr = [PackagesData]()
        for item in model{
            arr.append(PackagesData.init(dictionary: item as NSDictionary)!)
        }
        self.packageList = arr
        self.collectionView.reloadData()
    }
    func alertMsg(msg : String){
        IJProgressView.shared.hideProgressView();
        _ = SweetAlert().showAlert(NSLocalizedString("error", comment: ""), subTitle: msg, style: AlertStyle.error)
    }
}

class PackageItemCell : UICollectionViewCell{
    var imageCache = NSCache<AnyObject, AnyObject>()
    @IBOutlet weak var imgPackage: UIImageView!
    @IBOutlet weak var lblPackageDescription: UILabel!
    @IBOutlet weak var lblPackageName: UILabel!
    @IBOutlet weak var lblPackagePrice: UILabel!
    @IBOutlet weak var lblCreditCard: UILabel!
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)

    var model : PackagesData!{
        didSet{
            self.lblPackageName.text = model.title
            self.lblPackageDescription.text = model.description
            self.lblPackagePrice.text = String(describing: model.cost!) + " KWD"
            self.lblCreditCard.text = "Credit " + String(describing: model.credit!) + "KWD"
            
            if let imgURL = model.image{
                if let image = imageCache.object(forKey: imgURL as AnyObject) as? UIImage{
                    self.imgPackage.image = image
                }else{
                    let Url = String(format: imgURL)
                    guard let serviceUrl = URL(string: Url) else { return }
                    activityIndicator.startAnimating()
                    URLSession.shared.dataTask(with: serviceUrl, completionHandler: { (data, response, error) -> Void in
                        if error != nil{
                            return
                        }
                        let image = UIImage(data:data!)
                        self.imageCache.setObject(image!, forKey: imgURL as AnyObject)
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                            self.imgPackage.image = image
                        }
                    }).resume()
                }
            }
        }
    }
}
