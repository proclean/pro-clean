//
//  SettingTableViewController.swift
//  Pro Clean
//
//  Created by Nerneen Mohamed on 5/19/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import UIKit

class SettingTableViewController: UITableViewController {
var settingsName = ["My Profile","Saved address","Change password","Payment history","Messages","Notifications","Change language","Contact us","Sign out"]
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        if(UserDefaults().getDeviceLanguage() == "ar"){
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"side_menu"), style: .plain, target: self, action: #selector(openSideMenu(_:)))
            navigationItem.rightBarButtonItem?.tag = 1
        }else{
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"side_menu"), style: .plain, target: self, action: #selector(openSideMenu(_:)))
            navigationItem.leftBarButtonItem?.tag = 0
        }
        navigationItem.title = "Setting"
        
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
    }
    @objc public func openSideMenu(_ sender: AnyObject) {
        if(sender.tag == 0){
            self.sideMenuViewController?.presentLeftMenuViewController()
        }else{
            self.sideMenuViewController?.presentRightMenuViewController()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 9
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = settingsName[indexPath.row]
      
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        cell.backgroundColor = UIColor.groupTableViewBackground
        cell.selectionStyle = .none
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 0){
          //  cell.textLabel?.text = "My Profile"
        }else if(indexPath.row == 1){
          //  cell.textLabel?.text = "Saved address"
            Myaddress()
            
        }else if(indexPath.row == 2){
            self.openChangePassword()
          //  cell.textLabel?.text = "Change password"
            
        }else if (indexPath.row == 3){
           // cell.textLabel?.text = "Payment history"
            
        }else if (indexPath.row == 4){
          //  cell.textLabel?.text = "Messages"
            
        }else if (indexPath.row == 5){
          //  cell.textLabel?.text = "Notifications"
            
        }else if (indexPath.row == 6){
          //  cell.textLabel?.text = "Change language"
            
        }else if (indexPath.row == 7){
          //  cell.textLabel?.text = "Contact us"
            OurInfo()
            
        }else if (indexPath.row == 8){
         //   cell.textLabel?.text = "Sign out"
            signOut()
            
        }
    }
    func openChangePassword(){
        if(!UserDefaults().isLoggedIn()){
            let vc = UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "contentLoginVC")
            self.sideMenuViewController!.setContentViewController(vc, animated: true)
            self.sideMenuViewController!.hideMenuViewController()
        }else{
            let vc : ChangePasswordViewController = UIStoryboard(name: "User", bundle: nil).instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func Myaddress(){
        if(!UserDefaults().isLoggedIn()){
            let vc = UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "contentLoginVC")
            self.sideMenuViewController!.setContentViewController(vc, animated: true)
            self.sideMenuViewController!.hideMenuViewController()
        }else{
            let vc : AddressViewController = UIStoryboard(name: "User", bundle: nil).instantiateViewController(withIdentifier: "NewAddress") as! AddressViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func OurInfo(){
        let vc : ContactUsViewController = UIStoryboard(name: "User", bundle: nil).instantiateViewController(withIdentifier: "Info") as! ContactUsViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func signOut(){
        if let _ = UserDefaults().getUserDetails(){
            if(Utils.isInternetAvailable()){
                IJProgressView.shared.showProgressView(self.view)
                let reference = DIDDataHandler()
                let user = UserDefaults().getUserDetails()
                reference.signOut(delegate: self, userId: String(describing: user?.iD), token: (user?.token)!)
            }else{
                ViewControllerUtils.showDialogSingleBtn(view: self, title: "", msg: NSLocalizedString("noConnection", comment: "") ){(data) in
                }
            }
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SettingTableViewController : APIDelegate{
    func RequestEndDueToError(error: NSError) {
        //alert error
        IJProgressView.shared.hideProgressView();
        _ = SweetAlert().showAlert(NSLocalizedString("error", comment: ""), subTitle: error.localizedDescription, style: AlertStyle.error)
    }
    
    func signOut(model:String){
        IJProgressView.shared.hideProgressView();
        UserDefaults().removeObject(forKey: UserDefaultsKeys.userDetail.rawValue)
        UserDefaults().setUserToken(tID: "")
        UserDefaults().setIsLoggedIn(value: false)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateLoginView"), object: nil)
        //save data in db
            self.showHome()
        }

    func showHome(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : RootViewController = storyboard.instantiateViewController(withIdentifier: "rootController") as! RootViewController
        self.present(vc, animated: false, completion: nil)
    }

    func alertMsg(msg : String){
        IJProgressView.shared.hideProgressView();
        _ = SweetAlert().showAlert(NSLocalizedString("error", comment: ""), subTitle: msg, style: AlertStyle.error)
    }
}
