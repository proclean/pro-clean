//
//  ViewController.swift
//  NewGoogle
//
//  Created by Maged on 5/10/18.
//  Copyright © 2018 Mego. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreData
import CoreLocation
import GooglePlaces


class AddressViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {

    @IBOutlet weak var aders: UITextField!
    @IBOutlet weak var mapMarker: UIImageView!
    @IBOutlet weak var Adress: UITextField!
   var locationMarker: GMSMarker!
    @IBOutlet weak var Addressloc: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
  
    let locationManager = CLLocationManager()
 
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
       
        //Ask the user for location permision
        locationManager.requestWhenInUseAuthorization()

        mapView.delegate = self
       
    }
    //Fires when the user Allow/Doesn't allow the permission of getting the current location
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        //Check if the user allowed us to access the current location
        if status == .authorizedWhenInUse {
            
            //Start get the user's lcoation
            locationManager.startUpdatingLocation()
            
            //Show "My Current Location" to the user
            mapView.isMyLocationEnabled = false
            //mapView.settings.myLocationButton = true
        }
    }
    //Get the user location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first {
            //Setup the map camera
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 17, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
            
        }
        

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        
        // 1
        let geocoder = GMSGeocoder()
        // 2
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
                
            }
            print(lines)
            // 3
            
            self.Adress.text = lines.joined(separator: "\n")
            
            // 4
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
                
            }
        }
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
    }

    
}

