//
//  RegisterationViewController.swift
//  Pro Clean
//
//  Created by Nerneen Mohamed on 5/13/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import UIKit
import Toast_Swift
import ActionSheetPicker_3_0
import ReachabilitySwift
class RegisterationViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {

    @IBOutlet weak var etCity: BindingTextField!{
        didSet{
            etCity.bind{self.registerationViewModel.city = $0 }
        }
    }
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var lblSelectImage: UILabel!
    @IBOutlet weak var etPhoneNumber: UITextField!
    @IBOutlet weak var etCode: UITextField!
    @IBOutlet weak var etPassword: UITextField!
    @IBOutlet weak var etFullName: UITextField!
    @IBOutlet weak var etEmail: UITextField!
    @IBOutlet weak var imgUser: UIImageView!
    var arrRegionsData = [RegionsData]()
    var arrFilterationCity = [RegionsData]()
    let pickerView = UIPickerView()
    var selectedElement = 0
    var selectedCity : RegionsData!
    private var registerationViewModel : RegisterationViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.etEmail.delegate = self
        self.etPassword.delegate = self
        self.etFullName.delegate = self
        self.etPhoneNumber.delegate = self
        // Do any additional setup after loading the view.
       _ = Common.instance.circleImageView(self.imgUser)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"back_icon"), style: .plain, target: self, action: #selector(doBack(_:)))
        navigationItem.title = NSLocalizedString("register", comment: "")
        arrFilterationCity = arrRegionsData
        pickerView.delegate = self
       
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePicker(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePicker(_:)))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        etCity.inputView = pickerView
        etCity.inputAccessoryView = toolBar

        etCity.delegate = self
        self.registerationViewModel = RegisterationViewModel()
        APIUtils.reachabilityNotify(view: self,_selector: #selector(self.reachabilityChanged))

    }
    @objc func donePicker (_ sender:UIBarButtonItem){
        if selectedElement == 0{
            let row = pickerView.selectedRow(inComponent: 0);
            pickerView(pickerView, didSelectRow: row, inComponent:0)
        }
        self.registerationViewModel.city = self.etCity.text!
        self.view.endEditing(true)
    }
    @objc public func doBack(_ sender: AnyObject) {
         self.dismiss(animated: false, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func selectAvatarImage(_ sender: Any) {
        if sender is UIButton{
            let btn = sender as! UIButton
            self.imgUser.image =  btn.imageView?.image
            self.imgUser.tag = 1
            self.registerationViewModel.userimagename = btn.accessibilityIdentifier!
        }
    }
    @IBAction func doRegister(_ sender: Any) {
        self.registerationViewModel = RegisterationViewModel(fullname: self.etFullName.text!, password: self.etPassword.text!, email: self.etEmail.text!, phonenumber: self.etPhoneNumber.text!, phonenumberCode: self.etCode.text!, city: (self.etCity.text)!, userimagetag: self.imgUser.tag,userimagename:self.registerationViewModel.userimagename)
        if(self.registerationViewModel.isValid){
            if(Utils.isInternetAvailable()){
                IJProgressView.shared.showProgressView(self.view)
                let refrence = DIDDataHandler()
                var model = [String:Any]()
                model["full_name"] = self.etFullName.text!
                model["email"] = self.etEmail.text!
                model["password"] = self.etPassword.text!
                model["mobile"] = self.etCode.text! + self.etPhoneNumber.text!
                model["image"] = self.registerationViewModel.userimagename
                model["firebaseToken"] = ""
                model["regionID"] = String(describing: selectedCity.iD!)
                model["address"] = ""
              
                refrence.signup(delegate: self, model: model)
            }else{
                ViewControllerUtils.showDialogSingleBtn(view: self, title: "", msg: NSLocalizedString("noConnection", comment: "") ){(data) in
                }
            }
        }else{
            var msg = NSLocalizedString("emptyFields", comment: "")
            if(self.registerationViewModel.brokenRules.contains(where: { (item) -> Bool in
                item.propertyName == "emailFormateWrong"
            })){
                msg = NSLocalizedString("emailFormateWrong", comment: "")
            }
            if(self.registerationViewModel.brokenRules.contains(where: { (item) -> Bool in
                item.propertyName == "phoneFormateWrong"
            })){
                msg = NSLocalizedString("phoneFormateWrong", comment: "")
            }
            if(self.registerationViewModel.brokenRules.contains(where: { (item) -> Bool in
                item.propertyName == "userimage"
            })){
                msg = NSLocalizedString("avatarRequired", comment: "")
            }
            
            self.view.makeToast(msg)
        }
    }
    @IBAction func selectCity(_ sender: Any) {
        if (self.arrRegionsData.count > 0){
            ViewControllerUtils.popUpPicker(sender: UIButton(), rows: self.arrFilterationCity, _indexSelected: -1){ (data) in
                if let model = data as? [Any]{
                 
                }
            }
        }else{
            self.registerationViewModel.getRegionsData(controller: self)
        }
       
    }
    @objc func reachabilityChanged(note: Notification) {
        APIUtils.reachabilityChanged(view: self, note: note) { (result) in
            if(result){
                DispatchQueue.main.async {
                    IJProgressView.shared.showProgressView(self.view);
                    self.registerationViewModel.getRegionsData(controller: self)
                }
            }
        }
    }
}
extension RegisterationViewController:UITextFieldDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrFilterationCity.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if arrFilterationCity.count > 0 && row < arrFilterationCity.count{
            return arrFilterationCity[row].name
        }else{
            return ""

        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       
        if row != arrFilterationCity.count{
            etCity.text = arrFilterationCity[row].name
            selectedElement = row
            selectedCity = arrFilterationCity[row]
        }
        self.view.layoutIfNeeded()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.arrFilterationCity.removeAll()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            if (textField.text?.count)! > 0{
               self.arrFilterationCity = self.arrRegionsData.filter({ (item) -> Bool in
                (item.name?.lowercased().range(of:self.registerationViewModel.city.lowercased()) != nil)
                })
               
                

            self.pickerView.reloadAllComponents()
                
            }else{
                self.arrFilterationCity = self.arrRegionsData
                self.pickerView.reloadAllComponents()
            }
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

extension RegisterationViewController :APIDelegate{
    func registerationData(model:[String:Any]){
        IJProgressView.shared.hideProgressView();
        _ = SweetAlert().showAlert(NSLocalizedString("Successful", comment: ""), subTitle: "", style: AlertStyle.success)
        self.dismiss(animated: false, completion: nil)
    }
    func getRegionsData(model:[[String:Any]]){
        IJProgressView.shared.hideProgressView();
        var arr = [RegionsData]()
        for item in model{
            arr.append(RegionsData.init(dictionary: item as NSDictionary)!)
        }
        self.arrRegionsData = arr
        self.arrFilterationCity = self.arrRegionsData
    }
    func RequestEndDueToError(error: NSError) {
        //alert error
        IJProgressView.shared.hideProgressView();
        _ = SweetAlert().showAlert(NSLocalizedString("error", comment: ""), subTitle: error.localizedDescription, style: AlertStyle.error)
    }
    func alertMsg(msg : String){
        IJProgressView.shared.hideProgressView();
        _ = SweetAlert().showAlert(NSLocalizedString("error", comment: ""), subTitle: msg, style: AlertStyle.error)
    }
}
