//
//  ChangePasswordViewController.swift
//  Pro Clean
//
//  Created by Nerneen Mohamed on 5/14/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import UIKit
import TextFieldEffects

class ChangePasswordViewController: UIViewController {
    @IBOutlet weak var imgUSer: UIImageView!
    @IBOutlet weak var imgCoverUser: UIImageView!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var etRenewPassword: HoshiBindingTextField!{
        didSet{
            etRenewPassword.bind{self.changePasswordViewModel.renewPassword = $0 }
        }
    }
    @IBOutlet weak var etNewPassword: HoshiBindingTextField!{
        didSet{
            etNewPassword.bind{self.changePasswordViewModel.newPassword = $0 }
        }
    }
    @IBOutlet weak var etOldPassword: HoshiBindingTextField!{
        didSet{
            etOldPassword.bind { self.changePasswordViewModel.oldPassword = $0
            }
        }
    }
    
    private var changePasswordViewModel : ChangePasswordViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back_icon"), style: .plain, target: self, action: #selector(doBack))
        navigationItem.title = NSLocalizedString("changePassword", comment: "")
        
        self.changePasswordViewModel = ChangePasswordViewModel()
        // Do any additional setup after loading the view.
        setupData()
    }
    func setupData(){
        let userdata = UserDefaults().getUserDetails()
        self.imgUSer.image = UIImage(named: (userdata?.image!)!)
        self.imgCoverUser.image = UIImage(named: (userdata?.image!)!)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
    }
    
    @objc func doBack(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : RootViewController = storyboard.instantiateViewController(withIdentifier: "rootController") as! RootViewController
        self.present(vc, animated: false, completion: nil)
    }
    @IBAction func changePassword(){
        
        if(self.changePasswordViewModel.isValid){
            // self.changePasswordViewModel.register(caller: self)
            if(Utils.isInternetAvailable()){
                IJProgressView.shared.showProgressView(self.view)
                let refrence = DIDDataHandler()
                let user = UserDefaults().getUserDetails()
           let model =  ["current_password":self.changePasswordViewModel.oldPassword,
                "password":self.changePasswordViewModel.newPassword,
                "confirm_password":self.changePasswordViewModel.renewPassword,
                
                "userID":String(describing: user?.iD),
                "token":user?.token ] as [String : Any]
                refrence.changePassword(delegate: self, model: model)
            }else{
                ViewControllerUtils.showDialogSingleBtn(view: self, title: "", msg: NSLocalizedString("noConnection", comment: "") ){(data) in
                }
            }
            
        }else{
            var message = ""
            var existConfirmError = false
            for item in self.changePasswordViewModel.brokenRules{
                message += item.messager + "\n"
                if (item.propertyName == "confirmError"){
                    existConfirmError = true
                }
            }
            if(!existConfirmError){
                self.view.makeToast(NSLocalizedString("requiredFields", comment: ""))
            }else{ self.view.makeToast(NSLocalizedString("confirmError", comment: ""))
                
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
extension ChangePasswordViewController : APIDelegate{
    func changePassword(model:String){
        IJProgressView.shared.hideProgressView();
        
    }
    
    func RequestEndDueToError(error: NSError) {
        //alert error
        IJProgressView.shared.hideProgressView();
        _ = SweetAlert().showAlert(NSLocalizedString("error", comment: ""), subTitle: error.localizedDescription, style: AlertStyle.error)
    }
    func alertMsg(msg : String){
        IJProgressView.shared.hideProgressView();
        _ = SweetAlert().showAlert(NSLocalizedString("error", comment: ""), subTitle: msg, style: AlertStyle.error)
    }
}
