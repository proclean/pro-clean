//
//  LoginViewController.swift
//  Pro Clean
//
//  Created by Nerneen Mohamed on 5/13/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import UIKit
class LoginViewController: UIViewController {
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var btnForgetPassword: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var etEmail: BindingTextField!{
        didSet{
            etEmail.bind{self.loginViewModel.email = $0}
        }
    }
    @IBOutlet weak var etPassword: BindingTextField!{
        didSet{
            etPassword.bind{self.loginViewModel.password = $0 }
        }
    }
   
    var loginViewModel : LoginViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.etEmail.delegate = self
        self.etPassword.delegate = self
        loginViewModel = LoginViewModel()
        if(UserDefaults().getDeviceLanguage() == "ar"){
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"side_menu"), style: .plain, target: self, action: #selector(openSideMenu(_:)))
            navigationItem.rightBarButtonItem?.tag = 1
        }else{
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"side_menu"), style: .plain, target: self, action: #selector(openSideMenu(_:)))
            navigationItem.leftBarButtonItem?.tag = 0
        }
        setupData()
    }
    func setupData(){
        self.btnSkip.setTitle(NSLocalizedString("skip", comment: ""), for: .normal)
        self.btnSkip.addTarget(self, action: #selector(skip(_:)), for: .touchUpInside)
        self.btnForgetPassword.setTitle(NSLocalizedString("forgetPassword", comment: ""), for: .normal)
        self.btnRegister.setTitle(NSLocalizedString("newUser", comment: ""), for: .normal)
        self.btnForgetPassword.addTarget(self, action: #selector(forgetPassword_(_:)), for: .touchUpInside)
        
    }
    @objc func skip(_ button : UIButton){
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "contentHomeVC")
        self.sideMenuViewController!.setContentViewController(vc, animated: true)
        self.sideMenuViewController!.hideMenuViewController()
    }
    @objc func forgetPassword_(_ button : UIButton){

        let vc = UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "contentForgetPasswordVC")
        self.sideMenuViewController!.setContentViewController(vc, animated: true)
        self.sideMenuViewController!.hideMenuViewController()
        
      
        
    }
    @objc public func openSideMenu(_ sender: AnyObject) {
        if(sender.tag == 0){
            self.sideMenuViewController?.presentLeftMenuViewController()
        }else{
            self.sideMenuViewController?.presentRightMenuViewController()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func openRegisterationVC(_ sender: Any) {
        let storyboard = UIStoryboard(name: "User", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "contentRegisterVC")
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func login(_ sender: Any) {
        if(self.loginViewModel.isValid){
            if(Utils.isInternetAvailable()){
                IJProgressView.shared.showProgressView(self.view)
                let refrence = DIDDataHandler()
                refrence.login(delegate: self, email: self.etEmail.text!, _password: self.etPassword.text!)
            }else{
                ViewControllerUtils.showDialogSingleBtn(view: self, title: "", msg: NSLocalizedString("noConnection", comment: "") ){(data) in
                }
            }
        }else{
            var msg = NSLocalizedString("emptyFields", comment: "")
            if(self.loginViewModel.brokenRules.contains(where: { (item) -> Bool in
                item.propertyName == "emailFormateWrong"
            })){
                msg = NSLocalizedString("emailFormateWrong", comment: "")
            }
            self.view.makeToast(msg)
        }
    }
   
}
extension LoginViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
extension LoginViewController : APIDelegate{
    func setusetData(model:[String:Any]){
        IJProgressView.shared.hideProgressView();
        //save data in db
        self.loginViewModel.saveUserData(dict: model){(result) in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateLoginView"), object: nil)
            self.showHome()
        }
    }
    @objc func showHome(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : RootViewController = storyboard.instantiateViewController(withIdentifier: "rootController") as! RootViewController
        self.present(vc, animated: false, completion: nil)
    }
    func RequestEndDueToError(error: NSError) {
        //alert error
        IJProgressView.shared.hideProgressView();
        _ = SweetAlert().showAlert(NSLocalizedString("error", comment: ""), subTitle: error.localizedDescription, style: AlertStyle.error)
    }
    func alertMsg(msg : String){
        IJProgressView.shared.hideProgressView();
        _ = SweetAlert().showAlert(NSLocalizedString("error", comment: ""), subTitle: msg, style: AlertStyle.error)
    }
}
