//
//  ForgetPasswordViewController.swift
//  Pro Clean
//
//  Created by Nerneen Mohamed on 5/18/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: UIViewController {
    @IBOutlet weak var btnGoToLogin: UIButton!
    
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var etEmail: BindingTextField!{
        didSet{
            etEmail.bind{self.forgetPasswordViewModel.email = $0}
        }
    }
    var forgetPasswordViewModel : ForgetPasswordViewModel!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.etEmail.delegate = self

        forgetPasswordViewModel = ForgetPasswordViewModel()
        if(UserDefaults().getDeviceLanguage() == "ar"){
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"side_menu"), style: .plain, target: self, action: #selector(openSideMenu(_:)))
            navigationItem.rightBarButtonItem?.tag = 1
        }else{
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"side_menu"), style: .plain, target: self, action: #selector(openSideMenu(_:)))
            navigationItem.leftBarButtonItem?.tag = 0
        }
        setupData()
    }
    func setupData(){
        self.btnGoToLogin.setTitle(NSLocalizedString("backToLogin", comment: ""), for: .normal)
      
        self.btnSend.setTitle(NSLocalizedString("Send", comment: ""), for: .normal)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
       
    }
    
    @objc public func openSideMenu(_ sender: AnyObject) {
        if(sender.tag == 0){
            self.sideMenuViewController?.presentLeftMenuViewController()
        }else{
            self.sideMenuViewController?.presentRightMenuViewController()
        }
    }
    @IBAction func dismiss(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "contentLoginVC")
        self.sideMenuViewController!.setContentViewController(vc, animated: true)
        self.sideMenuViewController!.hideMenuViewController()
    }
    @IBAction func sendForgetPasswordEmail(_ sender: Any) {
        if(self.forgetPasswordViewModel.isValid){
            if(Utils.isInternetAvailable()){
                IJProgressView.shared.showProgressView(self.view)
                let refrence = DIDDataHandler()
                
               // refrence.login(delegate: self, email: self.etEmail.text!, _password: self.etPassword.text!)
            }else{
                ViewControllerUtils.showDialogSingleBtn(view: self, title: "", msg: NSLocalizedString("noConnection", comment: "") ){(data) in
                }
            }
        }else{
            var msg = NSLocalizedString("emptyFields", comment: "")
            if(self.forgetPasswordViewModel.brokenRules.contains(where: { (item) -> Bool in
                item.propertyName == "emailFormateWrong"
            })){
                msg = NSLocalizedString("emailFormateWrong", comment: "")
            }
            self.view.makeToast(msg)
        }
    }
}
extension ForgetPasswordViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
extension ForgetPasswordViewController : APIDelegate{
    func setusetData(model:[String:Any]){
        IJProgressView.shared.hideProgressView();
        //save data in db
//        self.loginViewModel.saveUserData(dict: model){(result) in
//            self.showHome()
//        }
    }
    @objc func showHome(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : RootViewController = storyboard.instantiateViewController(withIdentifier: "rootController") as! RootViewController
        self.present(vc, animated: false, completion: nil)
    }
    func RequestEndDueToError(error: NSError) {
        //alert error
        IJProgressView.shared.hideProgressView();
        _ = SweetAlert().showAlert(NSLocalizedString("error", comment: ""), subTitle: error.localizedDescription, style: AlertStyle.error)
    }
    func alertMsg(msg : String){
        IJProgressView.shared.hideProgressView();
        _ = SweetAlert().showAlert(NSLocalizedString("error", comment: ""), subTitle: msg, style: AlertStyle.error)
    }
}
