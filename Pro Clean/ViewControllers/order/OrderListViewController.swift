//
//  OrderListViewController.swift
//  Pro Clean
//
//  Created by Nerneen Mohamed on 5/14/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import UIKit
import ReachabilitySwift
protocol protocolOrder {
    func callDelegate(phoneDelegate : String)
}
enum orderStatus : String{
  
    case Current = "Current"
    case previous = "Previous"
    
    case waiting = "waiting"
    case cancel = "cancel"
    case submitted = "submitted"
    case in_progress = "in-progress"
    case assigned = "assigned"
    case completed = "completed"
    case delivering = "delivering"
}
class OrderListViewController: UIViewController ,protocolOrder{
    @IBOutlet weak var collectionView: UICollectionView!
    var status : String!
    var orderList = [OrderData]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupData()
        APIUtils.reachabilityNotify(view: self,_selector: #selector(self.reachabilityChanged))

    }
    // 0 -> Current
    // 1 -> Previous
    func configure(status:String){
        self.status = status
    }
    func setupData(){
        self.navigationItem.title = NSLocalizedString("MyOrders", comment: "")
        if(UserDefaults().getDeviceLanguage() == "ar"){
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"back_icon"), style: .plain, target: self, action: #selector(doBack(_:)))
            navigationItem.rightBarButtonItem?.tag = 1
        }else{
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"back_icon"), style: .plain, target: self, action: #selector(doBack(_:)))
            navigationItem.leftBarButtonItem?.tag = 0
        }
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    @objc public func doBack(_ sender: AnyObject) {
      self.dismiss(animated: false, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /// check network reachability
    ///
    /// - Parameter note: notify network change
    @objc func reachabilityChanged(note: Notification) {
        APIUtils.reachabilityChanged(view: self, note: note) { (result) in
            if(result){
                DispatchQueue.main.async {
                    IJProgressView.shared.showProgressView(self.view);
                    
                    let reference = DIDDataHandler()
                    let userdata = UserDefaults().getUserDetails()
                 
                    reference.getOrderList(delegate: self, userid: (String(describing: userdata?.iD!)), token: (userdata?.token)!, status: self.status)
                }
            }
        }
    }
    func callDelegate(phoneDelegate : String){
        let phone = "TEL://" + phoneDelegate
        let url: NSURL = URL(string: phone)! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }
   
}
extension OrderListViewController : APIDelegate{
    func RequestEndDueToError(error: NSError) {
        IJProgressView.shared.hideProgressView();
        _ = SweetAlert().showAlert(NSLocalizedString("error", comment: ""), subTitle: error.localizedDescription, style: AlertStyle.error)
    }
    func getOrderList(model:[[String:Any]]){
        IJProgressView.shared.hideProgressView();
        var arr = [OrderData]()
        for item in model{
            arr.append(OrderData.init(dictionary: item as NSDictionary)!)
        }
        self.orderList = arr
        for i in arr{
            print(i.status)
        }
        self.collectionView.reloadData()
    }
    func alertMsg(msg : String){
        IJProgressView.shared.hideProgressView();
        _ = SweetAlert().showAlert(NSLocalizedString("error", comment: ""), subTitle: msg, style: AlertStyle.error)
    }
}
extension OrderListViewController : UICollectionViewDelegate,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return orderList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderCollectionCell", for: indexPath) as! OrderCollectionCell
        cell.delegateOrder = self
        cell.configureCell()
        cell.model = self.orderList[indexPath.row]
       // cell.backgroundColor = UIColor.init(white: 0.95, alpha: 1)
        return cell
    }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
        }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: self.view.frame.size.width, height:180 )
    }
}
class OrderCollectionCell : UICollectionViewCell{
    var imageCache = NSCache<AnyObject, AnyObject>()
    var delegateOrder : protocolOrder!
    @IBOutlet weak var heightBtnOrder: NSLayoutConstraint!
    @IBOutlet weak var widthBtnorder: NSLayoutConstraint!
    @IBOutlet weak var viewDelegateInfo: UIView!
    @IBOutlet weak var btnDelegateImage: UIButton!
    @IBOutlet weak var lblDelegateName: UILabel!
    @IBOutlet weak var lblDelegatePhone: UILabel!
    @IBOutlet weak var btnOrderAction: UIButton!
    @IBOutlet weak var btnWaiting: UIButton!
    @IBOutlet weak var lblWaiting: UILabel!
    @IBOutlet weak var btnSubmited: UIButton!
    @IBOutlet weak var lblSubmited: UILabel!
    @IBOutlet weak var btnInProgress: UIButton!
    @IBOutlet weak var lblInProgress: UILabel!
    @IBOutlet weak var btnDelivery: UIButton!
    @IBOutlet weak var lblDelivery: UILabel!
    @IBOutlet weak var btnCompleted: UIButton!
    @IBOutlet weak var lblCompleted: UILabel!
    var status = -1
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    var arrBtns = [UIButton]()
    var arrLabels = [UILabel]()
    var model : OrderData? {
        didSet{
            switch model?.status {
            case orderStatus.waiting.rawValue:
                status = 0
                break
            case orderStatus.submitted.rawValue:
                status = 1
                break
            case orderStatus.in_progress.rawValue:
                status = 2
                break
            case orderStatus.delivering.rawValue:
                status = 3
                break
            case orderStatus.completed.rawValue:
                status = 4
                break
            case orderStatus.assigned.rawValue:
                status = 0
            default:
                status = 0
                break
            }
            var imgURL = ""
            if (model?.status == orderStatus.waiting.rawValue || model?.status == orderStatus.in_progress.rawValue || model?.status == orderStatus.submitted.rawValue){
                self.selectedStatus(tag: status)
                self.lblDelegateName.text = model?.incoming_delegate_name ?? ""
                self.lblDelegatePhone.text = model?.incoming_delegate_mobile ?? ""
                imgURL = (self.model?.incoming_delegate_image)!
                self.btnOrderAction.setImage(UIImage(named: "call"), for: .normal)
                self.btnOrderAction.addTarget(self, action: #selector(call), for: .touchUpInside)
                self.widthBtnorder.constant = 40
                self.heightBtnOrder.constant = 40

            }else if(model?.status == orderStatus.completed.rawValue){
                self.selectedStatus(tag: status)
                self.lblDelegateName.text = model?.incoming_delegate_name ?? ""
                self.lblDelegatePhone.text = model?.incoming_delegate_mobile ?? ""
                imgURL = (self.model?.incoming_delegate_image)!
                self.btnOrderAction.isHidden = true
            }
            else if (model?.status == orderStatus.assigned.rawValue){
                self.viewDelegateInfo.isHidden = true
                self.widthBtnorder.constant = 120
                self.heightBtnOrder.constant = 57
            }else{
                self.selectedStatus(tag: status)
                self.lblDelegateName.text = model?.outgoing_delegate_name ?? ""
                self.lblDelegatePhone.text = model?.outgoing_delegate_mobile ?? ""
                imgURL = (model?.outgoing_delegate_image)!
                self.btnOrderAction.setImage(UIImage(named: "call"), for: .normal)
                self.btnOrderAction.addTarget(self, action: #selector(call), for: .touchUpInside)
                self.widthBtnorder.constant = 50
                self.heightBtnOrder.constant = 50
            }
            btnDelegateImage.imageView?.image = nil
            activityIndicator.center = CGPoint(x: btnDelegateImage.bounds.size.width/2, y: btnDelegateImage.bounds.size.height/2)
            self.btnDelegateImage.addSubview(activityIndicator)
            
            if imgURL != ""{
                if let image = imageCache.object(forKey: imgURL as AnyObject) as? UIImage{
                    btnDelegateImage.imageView?.image = image
                }else{
                    let Url = String(format: imgURL)
                    guard let serviceUrl = URL(string: Url) else { return }
                    activityIndicator.startAnimating()
                    URLSession.shared.dataTask(with: serviceUrl, completionHandler: { (data, response, error) -> Void in
                        if error != nil{
                            return
                        }
                        let image = UIImage(data:data!)
                        self.imageCache.setObject(image!, forKey: imgURL as AnyObject)
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                            self.btnDelegateImage.setImage(image, for: .normal)
                        }
                    }).resume()
                }
            }
            
        }
    }
    func configureCell(){
       _ = Common.instance.circleView(self.btnDelegateImage)
        self.lblWaiting.text = NSLocalizedString("Wait", comment: "")
        self.lblDelivery.text = NSLocalizedString("Delivery", comment: "")
        self.lblSubmited.text = NSLocalizedString("submit", comment: "")
        self.lblCompleted.text = NSLocalizedString("complete", comment: "")
        self.lblInProgress.text = NSLocalizedString("InProgress", comment: "")
        
        self.btnWaiting.setImage(UIImage(named: "ic_waiting_sc"), for: .selected)
        self.btnWaiting.setImage(UIImage(named: "ic_waiting"), for: .normal)
        
        self.btnSubmited.setImage(UIImage(named: "ic_submit_sc"), for: .selected)
        self.btnSubmited.setImage(UIImage(named: "ic_submit"), for: .normal)
        self.btnInProgress.setImage(UIImage(named: "ic_progress_sc"), for: .selected)
        self.btnInProgress.setImage(UIImage(named: "ic_inProgress"), for: .normal)
        
        self.btnDelivery.setImage(UIImage(named: "ic_delivery_sc"), for: .selected)
        self.btnDelivery.setImage(UIImage(named: "ic_delivery"), for: .normal)
        
        self.btnCompleted.setImage(UIImage(named: "completed_sc"), for: .selected)
        self.btnCompleted.setImage(UIImage(named: "ic_complete"), for: .normal)
        

        arrBtns.append(btnWaiting)
        arrBtns.append(btnSubmited)
        arrBtns.append(btnInProgress)
        arrBtns.append(btnDelivery)
        arrBtns.append(btnCompleted)
        
        arrLabels.append(lblWaiting)
        arrLabels.append(lblSubmited)
        arrLabels.append(lblInProgress)
        arrLabels.append(lblDelivery)
        arrLabels.append(lblCompleted)

    }
    func selectedStatus(tag : Int){
        for i in 0...tag{
            arrBtns[i].isSelected = true
            arrLabels[i].textColor = UIColor.rgb(red: 86, green: 143, blue: 56)
        }
    }
    @objc func call(){
        
        self.delegateOrder.callDelegate(phoneDelegate : self.lblDelegatePhone.text!)
    }
    
}
