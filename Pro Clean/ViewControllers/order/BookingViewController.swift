//
//  BookingViewController.swift
//  Pro Clean
//
//  Created by Nerneen Mohamed on 5/20/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import UIKit

class BookingViewController: UIViewController {
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var lblEveningShift: UILabel!
    @IBOutlet weak var lblMorningShif: UILabel!
    @IBOutlet weak var etDiscount: TextField!
    @IBOutlet weak var etAddress: TextField!
    @IBOutlet weak var etRegion: TextField!
    @IBOutlet weak var btnEveningShift: CustomButton!
    @IBOutlet weak var btnMorningShift: CustomButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblUserBalance: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnBack.addTarget(self, action: #selector(doback(_:)), for: .touchUpInside)
        self.btnMorningShift.tag = 0
        self.btnEveningShift.tag = 1
        self.btnEveningShift.setImage(UIImage(named: "pm"), for: .normal)
        self.btnEveningShift.setImage(UIImage(named: "pm_sc"), for: .selected)

        self.btnMorningShift.setImage(UIImage(named: "am"), for: .normal)

        self.btnMorningShift.setImage(UIImage(named: "am_sc"), for: .selected)

        self.btnEveningShift.addTarget(self, action: #selector(selectShiftTime(_:)), for: .touchUpInside)
        self.btnMorningShift.addTarget(self, action: #selector(selectShiftTime(_:)), for: .touchUpInside)
        self.btnEveningShift.isSelected = true

    }
    override func viewDidAppear(_ animated: Bool) {
        let user = UserDefaults().getUserDetails()
        self.imgUser.image = UIImage(named: (user?.image!)!)
        self.lblUserName.text = user?.full_name!
        self.lblUserBalance.text = "Balance: " + (user?.balance)! + " KWD"
        self.etRegion.delegate = self
        self.etAddress.delegate = self
        self.etDiscount.delegate = self
    }
   @objc func selectShiftTime(_ btn : UIButton){
    if btn.tag == 0{
        self.btnMorningShift.isSelected = true
        self.btnEveningShift.isSelected = false
        self.lblEveningShift.textColor = .lightGray
        self.lblMorningShif.textColor = UIColor.rgb(red: 86, green: 143, blue: 56)
        self.btnMorningShift.BorderColor = UIColor.rgb(red: 86, green: 143, blue: 56)
        self.btnEveningShift.BorderColor = .lightGray
    }else{
        self.btnMorningShift.isSelected = false
        self.btnEveningShift.isSelected = true
        self.lblMorningShif.textColor = .lightGray
        self.lblEveningShift.textColor =  UIColor.rgb(red: 86, green: 143, blue: 56)

        self.btnEveningShift.BorderColor = UIColor.rgb(red: 86, green: 143, blue: 56)
        self.btnMorningShift.BorderColor = .lightGray
    }
    }
    @IBAction func doback(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : RootViewController = storyboard.instantiateViewController(withIdentifier: "rootController") as! RootViewController
        self.present(vc, animated: false, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BookingViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
