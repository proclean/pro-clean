//
//  orderPagerViewController.swift
//  Pro Clean
//
//  Created by Nerneen Mohamed on 5/15/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import UIKit
import Tabman
import Pageboy

class orderPagerViewController: TabmanViewController,UINavigationBarDelegate {
    private(set)  var viewControllers: [OrderListViewController] = [OrderListViewController]()
    var tabHeight : CGFloat  = 70.0
    weak var vc = OrderListViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
       
        navigationController?.isNavigationBarHidden = false
        viewControllers.removeAll()
        
        let navigationBar = navigationController!.navigationBar
        navigationItem.title = NSLocalizedString("MyOrders", comment: "")
        
        navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font : UIFont.systemFont(ofSize: 100)]
        
        let leftButton =  UIBarButtonItem(image: UIImage(named: "back_icon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(doBack(_:)))
        leftButton.tintColor = .white
      
      let model = ["   " + NSLocalizedString("currentOrders", comment: "") + "  "," " + NSLocalizedString("previousOrder", comment: "" + "     " )]
        
        navigationItem.leftBarButtonItem = leftButton
        for i in 0..<model.count{
            vc = childViewController(withTitle: model[i])
            if i == 0{
                vc?.configure(status: orderStatus.Current.rawValue)
            }else{
                vc?.configure(status: orderStatus.completed.rawValue)
            }
            viewControllers.append(vc!)
        }
       
        self.dataSource = self
        self.delegate = self
        self.bar.items = self.viewControllers.compactMap({
            Item(title: $0.title!.uppercased())
        })
        
        
        self.bar.style = .scrollingButtonBar
        
        // set appearance
        self.bar.appearance = TabmanBar.Appearance({ (appearance) in
            // colors
//            c9f6dd
//            201,246,221
            let color = UIColor.rgb(red: 201, green: 246, blue: 221)
            appearance.style.background = .solid(color: color)
            appearance.indicator.color = AppColor.TabBarColors.Selected
            appearance.state.selectedColor = AppColor.TabBarColors.Selected
            appearance.state.color = AppColor.TabBarColors.NotSelected
            // layout
            appearance.layout.height = .explicit(value: tabHeight)
            appearance.layout.interItemSpacing = 10.0
        })
        
    }
   @objc func doBack(_ button : UIButton){
       // self.navigationController?.popViewController(animated: false)
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let vc : RootViewController = storyboard.instantiateViewController(withIdentifier: "rootController") as! RootViewController
    self.present(vc, animated: false, completion: nil)
    }
    private func childViewController(withTitle title: String) -> OrderListViewController {
        let vc : OrderListViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OrderListViewController") as! OrderListViewController
        vc.title = "    " + title + "   "
        return vc
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   
}
extension orderPagerViewController: PageboyViewControllerDataSource   {
    // Data Source
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return viewControllers.count
    }
    
    func viewController(for pageboyViewController: PageboyViewController, at index: PageboyViewController.PageIndex) -> UIViewController? {
        return viewControllers[index]
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }
}
