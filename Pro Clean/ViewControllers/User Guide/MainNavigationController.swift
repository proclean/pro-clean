//
//  MainNavigationController.swift
//  LoginGuideWithCode
//
//  Created by Nerneen Mohamed on 4/3/18.
//  Copyright © 2018 Nerneen Mohamed. All rights reserved.
//

import Foundation
import UIKit
class MainNavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        UserDefaults().setDeviceLanguage(lang: NSLocale.current.languageCode!)
        if isshowGuideBefore(){
            perform(#selector(showHome), with: nil, afterDelay: 0.01)
        }else{
            perform(#selector(showGuide), with: nil, afterDelay: 0.01)
        }
    }
    @objc func showGuide(){
        let loginController = UserGuideViewController()
        present(loginController, animated: false, completion: nil)
    }
    @objc func showHome(){
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc : RootViewController = storyboard.instantiateViewController(withIdentifier: "rootController") as! RootViewController
                    self.present(vc, animated: false, completion: nil)
    }
    fileprivate func isLoggedIn() -> Bool{
        return UserDefaults.standard.isLoggedIn()
    }
    fileprivate func isshowGuideBefore() -> Bool{
        return UserDefaults.standard.isShowGuideBefore()
    }
    deinit {
        for item in self.view.subviews{
            item.removeFromSuperview()
        }
    }
}
