//
//  LoginCell.swift
//  LoginGuideWithCode
//
//  Created by Nerneen Mohamed on 4/2/18.
//  Copyright © 2018 Nerneen Mohamed. All rights reserved.
//

import UIKit
class LoginCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    let loginImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = UIImage(named:"logo")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    let emailTextField : LeftPaddedTextField = {
        let textfield = LeftPaddedTextField()
        textfield.placeholder = "Enter Email"
        textfield.layer.borderColor =  UIColor.lightGray.cgColor
        textfield.layer.borderWidth = 1
        textfield.keyboardType = .emailAddress
        return textfield
    }()
    let passwordTextfield : LeftPaddedTextField = {
        let textfield = LeftPaddedTextField()
        textfield.placeholder = "Enter password"
        textfield.layer.borderColor = UIColor.lightGray.cgColor
        textfield.layer.borderWidth = 1
        textfield.isSecureTextEntry = true
        return textfield
    }()
    lazy var loginButton : UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .orange
        button.setTitle("Log in", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(handelLogin), for: .touchUpInside)
        return button
    }()
  weak var delegateLogin : LoginControllerDelegate?
    @objc func handelLogin(){
        delegateLogin?.finishLogingIn()
    }
    func setupViews(){
        backgroundColor = .white
        addSubview(loginImageView)
        addSubview(emailTextField)
        addSubview(passwordTextfield)
        addSubview(loginButton)
        //center vertical
       _ = loginImageView.anchor(centerYAnchor, left: nil, bottom: nil, right: nil, topConstant: -230, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 160, heightConstant: 160)
        //center horizontal
        loginImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
      _ =  emailTextField.anchor(loginImageView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 8, leftConstant: 32, bottomConstant: 0, rightConstant: 32, widthConstant: 0, heightConstant: 50)
      _ =  passwordTextfield.anchor(emailTextField.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 16, leftConstant: 32, bottomConstant: 0, rightConstant: 32, widthConstant: 0, heightConstant: 50)
        _ = loginButton.anchor(passwordTextfield.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 16, leftConstant: 32, bottomConstant: 0, rightConstant: 32, widthConstant: 0, heightConstant: 50)
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class LeftPaddedTextField: UITextField {
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width + 10, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width + 10, height: bounds.height)
    }
}
