//
//  ViewController.swift
//  LoginGuideWithCode
//
//  Created by Nerneen Mohamed on 3/31/18.
//  Copyright © 2018 Nerneen Mohamed. All rights reserved.
//

import UIKit
protocol LoginControllerDelegate : class{
    func finishLogingIn()
}
class UserGuideViewController:  UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, LoginControllerDelegate {
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .white
        cv.dataSource = self
        cv.delegate = self
        cv.isPagingEnabled = true
        return cv
    }()
    
    let cellId = "cellId"
    let loginId = "LoginId"
    var pageControlBottomAnchor: NSLayoutConstraint?
    var skipButtonTopAnchor: NSLayoutConstraint?
    var nextButtonTopAnchor: NSLayoutConstraint?
    let pages : [Page] = {
       
        let mainHomePage = Page(title: NSLocalizedString("mainHome", comment: ""), message: NSLocalizedString("mainHomeDescription", comment: ""), imageName: "help1")
        
        let addnewOrderPage = Page(title: NSLocalizedString("addNewOrder", comment: ""), message: NSLocalizedString("addNewOrderDescription", comment: ""), imageName: "help2")
        
        let followOrderStatusPage = Page(title: NSLocalizedString("followOrderStatus", comment: ""), message: NSLocalizedString("followOrderStatusDescription", comment: ""), imageName: "help3")
        
        let menuPricesPage = Page(title: NSLocalizedString("priceList", comment: ""), message: NSLocalizedString("menuPriceDescription", comment: ""), imageName: "help4")

        return [mainHomePage, addnewOrderPage, followOrderStatusPage,menuPricesPage]
    }()
    lazy var pageControl : UIPageControl = {
        let pc = UIPageControl()
        pc.pageIndicatorTintColor = .lightGray
        pc.currentPageIndicatorTintColor = UIColor(red: 247/255, green: 154/255, blue: 27/255, alpha: 1)
        pc.numberOfPages = self.pages.count
        if(UserDefaults().getDeviceLanguage() == "ar"){
            pc.transform = CGAffineTransform(scaleX: -1, y: 1);
        }
        return pc
    }()
    lazy var skipButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Skip", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
//        button.setTitleColor(UIColor(red: 247/255, green: 154/255, blue: 27/255, alpha: 1), for: .normal)
        button.addTarget(self, action: #selector(skip), for: .touchUpInside)
        return button
    }()
    @objc func skip(){
        // we only need to lines to do this
      //  pageControl.currentPage = pages.count - 1
        openHome()
      //  nextPage()
    }
    lazy var nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Next", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
//        button.setTitleColor(UIColor(red: 247/255, green: 154/255, blue: 27/255, alpha: 1), for: .normal)
        button.addTarget(self, action: #selector(nextPage), for: .touchUpInside)
        return button
    }()
    @objc func nextPage(){
        //we are on the last page
        if pageControl.currentPage == pages.count - 1{
            return
        }
        var pageIndex = pages.count - 1
        if(UserDefaults().getDeviceLanguage() == "ar"){
            pageIndex = 1
        }
        
        if pageControl.currentPage == pageIndex{
            moveControlConstraintsOffScreen()
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
            
        }
        //next page
                    let indexPath = IndexPath(item: pageControl.currentPage + 1, section: 0)
                    collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                    pageControl.currentPage += 1
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(collectionView)
        view.addSubview(pageControl);
        view.addSubview(skipButton)
        view.addSubview(nextButton)
        
        
        pageControlBottomAnchor = pageControl.anchor(nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 40)[1]
        
          skipButtonTopAnchor = skipButton.anchor(nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 0, widthConstant: 60, heightConstant: 50).first
        
        nextButtonTopAnchor = nextButton.anchor(nil, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 16, widthConstant: 60, heightConstant: 50).first

        //use autolayout instead
        collectionView.anchorToTop(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor)
        registerCells()
        UserDefaults().setIsShowGuideBefore(value: true)
    }
    fileprivate func registerCells(){
        collectionView.register(PageCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(LoginCell.self, forCellWithReuseIdentifier: loginId)
    }
    private func observeKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: .UIKeyboardWillHide, object: nil)
    }
    
   @objc func keyboardHide() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            
        }, completion: nil)
    }
    
   @objc func keyboardShow() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            let y: CGFloat = UIDevice.current.orientation.isLandscape ? -100 : -50
            self.view.frame = CGRect(x: 0, y: y, width: self.view.frame.width, height: self.view.frame.height)
            
        }, completion: nil)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageNumber = targetContentOffset.pointee.x / view.frame.width
        pageControl.currentPage = Int(pageNumber)
        var pageIndex = pages.count - 1
        if(UserDefaults().getDeviceLanguage() == "ar"){
            pageIndex = 0
        }
        if Int(pageNumber) == pageIndex {
            moveControlConstraintsOffScreen()
        }else{
            pageControlBottomAnchor?.constant = 0
            nextButtonTopAnchor?.constant = 0
            skipButtonTopAnchor?.constant = 16
        }
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    fileprivate func moveControlConstraintsOffScreen() {
        pageControlBottomAnchor?.constant = 50
        skipButtonTopAnchor?.constant = -50
        nextButtonTopAnchor?.constant = 50
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pages.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        if indexPath.item == pages.count{
//            let loginCell = collectionView.dequeueReusableCell(withReuseIdentifier: loginId, for: indexPath) as! LoginCell
//            loginCell.delegateLogin = self
//            return loginCell
//        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PageCell
        let page = pages[indexPath.row]
        cell.page = page
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        print(UIDevice.current.orientation.isLandscape)
        collectionView.collectionViewLayout.invalidateLayout()
        let indexPath = NSIndexPath(item: pageControl.currentPage, section: 0)
        // scroll to index path after the rotation is going
        DispatchQueue.main.async {
            self.collectionView.scrollToItem(at: indexPath as IndexPath, at: .centeredHorizontally, animated: true)
            self.collectionView.reloadData()
        }
    }
    func finishLogingIn(){
        print("Finish log in from Login Controller")
        let rootController = UIApplication.shared.keyWindow?.rootViewController
        guard let mainViewController = rootController as? MainNavigationController else {
            return
        }
        mainViewController.viewControllers = [HomeViewController()]
        dismiss(animated: true, completion: nil)
    }
    func openHome(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : RootViewController = storyboard.instantiateViewController(withIdentifier: "rootController") as! RootViewController
        self.present(vc, animated: true, completion: nil)
    }
    deinit{
        
    }
}
