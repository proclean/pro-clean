//
//  Page.swift
//  LoginGuideWithCode
//
//  Created by Nerneen Mohamed on 3/31/18.
//  Copyright © 2018 Nerneen Mohamed. All rights reserved.
//

import Foundation
struct Page {
    let title : String
    let message : String
    let imageName : String
}
