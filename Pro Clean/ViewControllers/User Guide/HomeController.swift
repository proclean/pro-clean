//
//  HomeController.swift
//  LoginGuideWithCode
//
//  Created by Nerneen Mohamed on 4/3/18.
//  Copyright © 2018 Nerneen Mohamed. All rights reserved.
//

import Foundation
import UIKit
import ImageSlideshow
import Alamofire
class HomeViewController: UIViewController {
    @IBOutlet weak var viewRegister: UIView!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var lblSetting: UILabel!
    @IBOutlet weak var lblUserGuide: UILabel!
    @IBOutlet weak var lblPriceMenu: UILabel!
    @IBOutlet weak var lblNewOrder: UILabel!
    @IBOutlet weak var lblPromoCode: UILabel!
    @IBOutlet weak var lblOrder: UILabel!
    @IBOutlet weak var slideshow: ImageSlideshow!
   
    var localSource = Array<KingfisherSource>()

    override func viewDidLoad() {
        super.viewDidLoad()
        bindData()
        let reference = DIDDataHandler()
        reference.getSliderImages(delegate: self)
    }
    
    func bindData(){
        slideshow.activityIndicator = DefaultActivityIndicator(style: .gray, color: UIColor.gray )
        slideshow.setImageInputs(localSource)
        slideshow.slideshowInterval = 3.0
        self.slideshow.reloadInputViews()
        
        self.lblOrder.text = NSLocalizedString("MyOrders", comment: "")
        self.lblPromoCode.text = NSLocalizedString("packages", comment: "")
        self.lblNewOrder.text = NSLocalizedString("booking", comment: "")
        self.lblPriceMenu.text = NSLocalizedString("priceList", comment: "")
        self.lblUserGuide.text = NSLocalizedString("userGuide", comment: "")
        self.lblSetting.text = NSLocalizedString("setting", comment: "")
        self.btnRegister.setTitle(NSLocalizedString("newUser", comment: ""), for: .normal) 
        
        if(UserDefaults().getDeviceLanguage() == "ar"){
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"side_menu"), style: .plain, target: self, action: #selector(openSideMenu(_:)))
            navigationItem.rightBarButtonItem?.tag = 1
        }else{
            let btn = UIBarButtonItem(image: UIImage(named:"side_menu"), style: .plain, target: self, action: #selector(openSideMenu(_:)))
//            navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"side_menu"), style: .plain, target: self, action: #selector(openSideMenu(_:)))
//            navigationItem.leftBarButtonItem?.tag = 0
            
            let button = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 44))
            let label = UILabel(frame: CGRect(x: 0, y: 15, width: 60, height: 16)) // adjust as you see fit
            
            label.text = "Home"
            label.font = UIFont.systemFont(ofSize: 20)
            label.textColor = .white
            label.textAlignment = NSTextAlignment.left
            
            button.addSubview(label)
            
            // Add it to your left bar button
            
            self.navigationItem.leftBarButtonItems = [btn, UIBarButtonItem(customView: button)]
                navigationItem.leftBarButtonItem?.tag = 0

        }
            self.btnRegister.isHidden = UserDefaults().isLoggedIn()
            self.viewRegister.isHidden = UserDefaults().isLoggedIn()
    }
    @objc public func openSideMenu(_ sender: AnyObject) {
        if(sender.tag == 0){
            self.sideMenuViewController?.presentLeftMenuViewController()
        }else{
            self.sideMenuViewController?.presentRightMenuViewController()
        }
    }
    @objc func handelSignOut(){
        UserDefaults.standard.setIsLoggedIn(value: false)
        let loginController = UserGuideViewController()
        present(loginController, animated: true, completion: nil)
    }
    @IBAction func openRegisterVC(_ sender: Any) {
        let vc = UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "contentRegisterVC")
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func openmyOrders(_ sender: Any) {
        if(!UserDefaults().isLoggedIn()){
            let vc = UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "contentLoginVC")
                self.sideMenuViewController!.setContentViewController(vc, animated: true)
                self.sideMenuViewController!.hideMenuViewController()
        }else{
            let vc : orderPagerViewController = UIStoryboard(name: "Pager", bundle: nil).instantiateViewController(withIdentifier: "orderPagerViewController") as! orderPagerViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func getPackages(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "contentPackagesListVC")
        self.sideMenuViewController!.setContentViewController(vc, animated: true)
        self.sideMenuViewController!.hideMenuViewController()
    }
    
    @IBAction func doBooking(_ sender: Any) {
        if(!UserDefaults().isLoggedIn()){
            let vc = UIStoryboard.init(name: "User", bundle: nil).instantiateViewController(withIdentifier: "contentLoginVC")
            self.sideMenuViewController!.setContentViewController(vc, animated: true)
            self.sideMenuViewController!.hideMenuViewController()
        }else{
            let vc  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BookingViewController")
            self.present(vc, animated: false, completion: nil)
         //   self.sideMenuViewController!.setContentViewController(vc, animated: true)
          //  self.sideMenuViewController!.hideMenuViewController()
        }
    }
    
    @IBAction func getPriceList(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "contentPriceListVC")
        
        self.sideMenuViewController!.setContentViewController(vc, animated: true)
        self.sideMenuViewController!.hideMenuViewController()
    }
    @IBAction func openUserGuide(_ sender: Any) {
        let loginController = UserGuideViewController()
        present(loginController, animated: false, completion: nil)
    }
    
    @IBAction func openSetting(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "contentSettingVC")
        
        self.sideMenuViewController!.setContentViewController(vc, animated: true)
        self.sideMenuViewController!.hideMenuViewController()
        
    }
}
extension HomeViewController : APIDelegate{
    func RequestEndDueToError(error: NSError) {
        
    }
    func getSlidesData(model:[[String:Any]]){
        for item in model {
            var imgUrl = item["image"] as! String
            imgUrl = imgUrl.replacingOccurrences(of: "/./", with: "/")
            self.localSource.append(KingfisherSource(urlString: imgUrl)!)
            slideshow.setImageInputs(localSource)
            self.slideshow.reloadInputViews()
        }
    }
}
