//filebreak:PageCell
import UIKit

class PageCell: UICollectionViewCell {
    var page : Page?{
        didSet{
            guard let page = page else {
                return
            }
            var imageName = page.imageName
            if UIDevice.current.orientation.isLandscape {
                imageName += "_landscape"
            }
            imageView.image = UIImage(named:imageName)
            
//            let color = UIColor.init(white: 0.2, alpha: 1)
            let color = UIColor.init(white: 0.95, alpha: 1)

            let attributedText = NSAttributedString(string: page.title, attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 25, weight: .medium),NSAttributedStringKey.foregroundColor: color])
            
            let attributeMessageText = NSAttributedString(string: "\n\n \(page.message)", attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 16),NSAttributedStringKey.foregroundColor: color])
           
            
            let combination = NSMutableAttributedString()
            
            combination.append(attributedText)
            combination.append(attributeMessageText)
            
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .center
            let length = combination.string.count;
            
         combination.addAttributes([NSAttributedStringKey.paragraphStyle: paragraphStyle], range: NSRange(location: 0, length: length))
            textView.attributedText = combination

       }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.backgroundColor = UIColor.groupTableViewBackground
        iv.image = UIImage(named: "help1")
        iv.clipsToBounds = true
        return iv
    }()
    let textView : UITextView = {
    let textView = UITextView()
        textView.text = "SAMPLE TEXT"
        textView.isEditable = false
        textView.backgroundColor = .clear
        textView.contentInset = UIEdgeInsets(top: 24, left: 0, bottom: 0, right: 0)
        return textView
    }()
    let lineSeperatorView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white:0.9,alpha:1)
        return view
    }()
    
    func setupViews() {
        addSubview(imageView)
        addSubview(textView)
        addSubview(lineSeperatorView)
        imageView.anchorToTop(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor)
        textView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true

         textView.anchorWithConstantsToTop(centerYAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: -50, leftConstant: 16, bottomConstant: 0, rightConstant: 16)

        //center horizontal
        textView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
                
        textView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.3).isActive = true
        
        _ = lineSeperatorView.anchor(nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 50, rightConstant: 0, widthConstant: 400, heightConstant: 50).first

        lineSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
