//
//  AppColor.swift
//  Pro Clean
//
//  Created by Nerneen Mohamed on 5/15/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import Foundation
import Foundation
import UIKit
struct AppColor {
    struct TabBarColors{
        static let Selected = UIColor.rgb(red: 0, green: 102, blue: 0)
        static let NotSelected = UIColor.rgb(red: 86, green: 143, blue: 56)
    }

}
