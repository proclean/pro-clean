//
//  UserDefaults + helpers.swift
//  LoginGuideWithCode
//
//  Created by Nerneen Mohamed on 4/3/18.
//  Copyright © 2018 Nerneen Mohamed. All rights reserved.
//

import Foundation
public enum UserDefaultsKeys: String {
    case isLoggedIn
    case deviceLanguage
    case userToken
    case userDetail
    case userAdresses
    case isShowGuideDone
}
extension UserDefaults {
  
    
    func setIsLoggedIn(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
        synchronize()
    }
    
    func isLoggedIn() -> Bool {
        return bool(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    func setIsShowGuideBefore(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isShowGuideDone.rawValue)
        synchronize()
    }
    
    func isShowGuideBefore() -> Bool {
        return bool(forKey: UserDefaultsKeys.isShowGuideDone.rawValue)
    }
    func setDeviceLanguage(lang : String){
        set(lang,forKey:UserDefaultsKeys.deviceLanguage.rawValue)
        synchronize()
    }
    
    func getDeviceLanguage() -> String! {
        if(isKeyPresentInUserDefaults(key: UserDefaultsKeys.deviceLanguage.rawValue)){
            return UserDefaults.standard.string(forKey: UserDefaultsKeys.deviceLanguage.rawValue)
        }
        return nil
    }
    func setUserToken(tID:String!) {
        UserDefaults.standard.set(tID, forKey: UserDefaultsKeys.userToken.rawValue)
    }
    
    func getUserToken() -> String? {
        if(isKeyPresentInUserDefaults(key:UserDefaultsKeys.userToken.rawValue)){
            return UserDefaults.standard.string(forKey: UserDefaultsKeys.userToken.rawValue)
        }
        return nil
    }
    
    func setUserDetails(userObj:UserData) {
        let data = NSKeyedArchiver.archivedData(withRootObject: userObj)
        UserDefaults().set(data, forKey: UserDefaultsKeys.userDetail.rawValue)
        synchronize()
    }
    func getUserDetails() -> UserData? {
        var objUser:UserData?
        if let data = UserDefaults().object(forKey: UserDefaultsKeys.userDetail.rawValue) as? Data {
            objUser = NSKeyedUnarchiver.unarchiveObject(with: data) as? UserData
        }
        return (objUser ?? nil)
    }
    
    func setUserAddresses(userAddresses:[Addresses]) {
        let userDefaults = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userAddresses)
        userDefaults.set(encodedData, forKey: UserDefaultsKeys.userAdresses.rawValue)
        userDefaults.synchronize()
    }
    func getUserAddresses() -> [Addresses]? {
        var objUser:[Addresses]?
        if let data = UserDefaults().object(forKey: UserDefaultsKeys.userAdresses.rawValue) as? Data {
            objUser = NSKeyedUnarchiver.unarchiveObject(with: data) as?  [Addresses]
        }
        return (objUser ?? nil)
    }
    
    
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
}
