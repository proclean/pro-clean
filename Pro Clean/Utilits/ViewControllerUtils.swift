//
//  ViewControllerUtils.swift
//  iTeacherApp
//
//  Created by Smart Tech on 5/1/17.
//  Copyright © 2017 Smart Tech. All rights reserved.
//

import Foundation
import UIKit
import ActionSheetPicker_3_0
import PopupDialog
import os
class ViewControllerUtils {
    
    @IBOutlet weak var lblQOrder: UILabel!
    var container: UIView = UIView()
    @IBOutlet weak var satckView: UIStackView!
    @IBOutlet weak var scrollView: UIScrollView!
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    //constant
    
    /*
     Show customized activity indicator,
     actually add activity indicator to passing view
     
     @param uiView - add activity indicator to this view
     */
    func showActivityIndicator(uiView: UIView) {
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColorFromHex(rgbValue: 0xffffff, alpha: 0.3)
        
        loadingView.frame = CGRectMake(0, 0, 80, 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColorFromHex(rgbValue: 0x444444, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2,y :loadingView.frame.size.height / 2)
        
        loadingView.addSubview(activityIndicator)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        activityIndicator.startAnimating()
    }
    
    /*
     Hide activity indicator
     Actually remove activity indicator from its super view
     
     @param uiView - remove activity indicator from this view
     */
    func hideActivityIndicator(uiView: UIView) {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
        
        
    }
    
    /*
     Define UIColor from hex value
     
     @param rgbValue - hex color value
     @param alpha - transparency level
     */
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    func CGRectMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect {
        return CGRect(x: x, y: y, width: width, height: height)
    }
    
    
    public  func setupButtonRound(btn : UIButton){
        btn.layer.cornerRadius = 5
        btn.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        btn.layer.shadowOffset = CGSize(width: 0, height: 3)
        btn.layer.shadowOpacity = 1.0
        btn.layer.shadowRadius = 5
        btn.layer.masksToBounds = false
    }
    
    public static  func showDialogSingleBtn(view : UIViewController , title : String , msg : String ,onCompletion: @escaping (Any) -> Void ){
        let title = title
        let message = msg

        // Create the dialog
        let popup = PopupDialog(title: title, message: message, buttonAlignment: .horizontal, transitionStyle: .zoomIn, gestureDismissal: false) {
        }




        // Create second button
        let buttonTwo = DefaultButton(title: NSLocalizedString("done", comment: "")) {
            //  self.label.text = "You ok'd the default dialog"
            onCompletion("btn")
        }

        // Add buttons to dialog
        popup.addButtons([ buttonTwo])

        // Present dialog
        view.present(popup, animated: true, completion: nil)
        
    }
    
    
    static func popUpPicker(sender : UIButton,rows: [
        Any],_indexSelected:Int , onCompletion: @escaping (Any) -> Void) {
        ActionSheetMultipleStringPicker.show(withTitle: NSLocalizedString("select", comment: ""), rows: [rows], initialSelection: [_indexSelected,_indexSelected], doneBlock: {
            picker, indexes, values in
            picker?.setTextColor(Utils.getUIColor(red: 35, green: 96, blue: 147, alpha: 1))
            var arr = [Any]()
            arr.append(ViewControllerUtils.setIndexPopUpPicker(index: (indexes?.description)!))
            arr.append(ViewControllerUtils.setSelectedValuePopUpPicker(value: String(describing: values)))
            onCompletion(arr)
            return
            
        }, cancel: {
            ActionMultipleStringCancelBlock in return
            onCompletion(false)
        }, origin: sender )
    }
    public static  func showDialogDoubleBtn(fromview : UIViewController ,toview : UIViewController, title : String , msg : String ,onCompletion: @escaping (Bool) -> Void ){
        
        // Create the dialog
        
//        let popup = PopupDialog(viewController: toview, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: false)
//        
//        let buttonOne = CancelButton(title: Utils.localizeText(key : "cancel"), height: 60) {
//            onCompletion(false)
//        }
//        
//        let buttonTwo = DefaultButton(title: Utils.localizeText(key : "done"), height: 60) {
//            onCompletion(true)
//        }
//        popup.addButtons([buttonOne, buttonTwo])
//        
//        fromview.present(popup, animated: true, completion: nil)
        
    }
    
    
    public static  func showAlert(view : UIViewController , title : String , msg : String,onCompletion: @escaping (Bool) -> Void){
        let title = title
        let message = msg
        // Create the alert
        let alert = UIAlertController(title: title, message:message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertActionStyle.default, handler: { action in
            onCompletion(true)
        }))
        
        view.present(alert, animated: true, completion: nil)
    }
    public static  func showErrorAlert(title : String , msg : String,onCompletion: @escaping (Bool) -> Void){
        let title = title
        let message = msg
        // Create the alert
        let errorAlert = UIAlertController(title: title, message:message, preferredStyle: UIAlertControllerStyle.alert)
        
        errorAlert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertActionStyle.default, handler: { action in
            onCompletion(true)
        }))
        UIApplication.shared.keyWindow?.rootViewController?.present(errorAlert, animated: true, completion: nil)
    }
    public static  func showAlertWithCancel(view : UIViewController , title : String , msg : String,onCompletion: @escaping (Bool) -> Void){
        let title = title
        let message = msg
        // Create the alert
        let alert = UIAlertController(title: title, message:message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: UIAlertActionStyle.cancel, handler:{ action in
            onCompletion(false)
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertActionStyle.default, handler: { action in
            onCompletion(true)
        }))
        
        view.present(alert, animated: true, completion: nil)
    }
    
    
   
   
    public static func removeControllers(view : UIViewController,index: Int)
    {
        let arr = view.navigationController!.viewControllers as Array
        for i in index..<arr.count - 1
        {
            view.navigationController?.viewControllers.remove(at: i)
        }
    }
    static func setIndexPopUpPicker(index : String) -> Int
    {
        var indexStr = index
        indexStr = indexStr.replacingOccurrences(of: "[", with: "")
        indexStr = indexStr.replacingOccurrences(of: "]", with: "")
        return Int(indexStr)!
        
    }
    static func setSelectedValuePopUpPicker(value : String) -> String
    {
        var s = value
        s = s.components(separatedBy: "(").last!;
        s = s.replacingOccurrences(of: ")", with: "")
        s = s.replacingOccurrences(of: "\n", with: "")
        return s
    }
//    static func popUpPicker(sender : UIButton,rows: [
//        Any],_indexSelected:Int , onCompletion: @escaping (Any) -> Void) {
//        ActionSheetMultipleStringPicker.show(withTitle: Utils.localizeText(key: "select"), rows: [rows], initialSelection: [_indexSelected,_indexSelected], doneBlock: {
//            picker, indexes, values in
//            picker?.setTextColor(Utils.getUIColor(red: 35, green: 96, blue: 147, alpha: 1))
//            var arr = [Any]()
//            arr.append(ViewControllerUtils.setIndexPopUpPicker(index: (indexes?.description)!))
//            arr.append(ViewControllerUtils.setSelectedValuePopUpPicker(value: String(describing: values)))
//            onCompletion(arr)
//            return
//
//        }, cancel: {
//            ActionMultipleStringCancelBlock in return
//            onCompletion(false)
//        }, origin: sender )
//    }

    
    public static func changePassword(viewController : UIViewController)
    {
        var valid = true
        var oldPassword = ""
        var newPassword = ""
        var confirmPassword = ""
        let alertController = UIAlertController(title: NSLocalizedString("changePassword", comment: ""), message: "", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: NSLocalizedString("changePassword", comment: ""), style: .default) { (_) in
                        if let txtOld = alertController.textFields?[0]  {
                            oldPassword = txtOld.text!
                        }
            if let txtNew = alertController.textFields?[1]  {
                newPassword = txtNew.text!
            }
            if let txtConfirm = alertController.textFields?[2]  {
                confirmPassword = txtConfirm.text!
            }
            oldPassword = oldPassword.replacingOccurrences(of: " ", with: "")
            newPassword = newPassword.replacingOccurrences(of: " ", with: "")
            confirmPassword = confirmPassword.replacingOccurrences(of: " ", with: "")
            if(oldPassword == "" || newPassword == "" || confirmPassword == ""){
                valid = false
            }
            if(confirmPassword != newPassword){
                valid = false
            }
            if(valid){

                valid = true
                let etTxtOld =  alertController.textFields![0]
                let etTxtNew =  alertController.textFields![1]
                let etTxtConfirm =  alertController.textFields![2]

                if(oldPassword == ""){
                    Utils.MakeBoarderwithCustomColor(view: etTxtOld, color: .red,cornerRadius: 5,borderWidth: 1)
                }else{
                    etTxtOld.layer.borderWidth = 0
                }
                if(newPassword == ""){
                    Utils.MakeBoarderwithCustomColor(view: etTxtNew, color: .red,cornerRadius: 5,borderWidth: 1)
                }else{
                    etTxtNew.layer.borderWidth = 0
                }
                if(confirmPassword == ""){
                    Utils.MakeBoarderwithCustomColor(view: etTxtConfirm, color: .red,cornerRadius: 5,borderWidth: 1)
                }else{
                    etTxtConfirm.layer.borderWidth = 0
                }
                if(confirmPassword != newPassword){
//                    alertController.view.makeToast(Utils.localizeText(key: "matchConfirmNewPasswordMsg"))
                    Utils.MakeBoarderwithCustomColor(view: etTxtConfirm, color: .red,cornerRadius: 5,borderWidth: 1)
                }else{
                    etTxtConfirm.layer.borderWidth = 0
                }
                viewController.present(alertController, animated: true, completion: nil)
            }
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel) { (_) in }
        
        alertController.addTextField { (textField) in
//            textField.placeholder = Utils.localizeText(key: "enterOldPassword")
            textField.isSecureTextEntry = true
        }
        alertController.addTextField { (textField) in
//            textField.placeholder = Utils.localizeText(key: "enterNewPassword")
            textField.isSecureTextEntry = true
        }
        alertController.addTextField { (textField) in
//            textField.placeholder = Utils.localizeText(key: "enterConfirmPassword")
            textField.isSecureTextEntry = true
        }
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    //Dynamic Nibs
    static func addRowToStackView(_caller : UIViewController, _stackView :UIStackView,_view : UIView ,_height : CGFloat)
    {
        _view.heightAnchor.constraint(equalToConstant: _height).isActive = true
        _view.widthAnchor.constraint(equalToConstant: _caller.view.frame.size.width).isActive = true
        _stackView.addArrangedSubview(_view)
    }
    
}


