//
//  Utils.swift
//  Unilever
//
//  Created by Smart Tech on 6/15/17.
//  Copyright © 2017 Smart tech. All rights reserved.
//

import UIKit
import ReachabilitySwift
import Foundation
import SystemConfiguration

class Utils: NSObject {
    static func getApplicationVersion() -> [String]? {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        var arr = [String]()
        arr.append(version)
        arr.append(build)
        return arr
    }
    static func customLogger(message : String,level :Int){
        let currentLevel = sDefaults().getCurrentPrintLevel() ?? -1
        if(level > 0 && level <= currentLevel){
            print(message)
        }
    }
    static func textAutoHeight(lbl : UILabel , text: String) -> Int{
        
        lbl.frame = CGRect(x : lbl.frame.origin.x,y :lbl.frame.origin.y,width : lbl.frame.width ,height : Utils.heightForView(lbl : lbl,text: text, width: lbl.frame.width))
        return  Int(lbl.frame.height)//Int(Utils.heightForView(text: text, width: 400))
    }
    
    static func heightForView(lbl : UILabel ,text:String, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: lbl.frame.origin.x, y: lbl.frame.origin.y, width: lbl.frame.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        //label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    
    //HRLPER FUNCTIONS
    static func CGRectMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect {
        return CGRect(x: x, y: y, width: width, height: height)
    }
    
    
    static func createView(x : CGFloat , y : CGFloat , w: CGFloat , h : CGFloat) -> UIView {
        let myRect = CGRect(x: x, y: y, width: w, height: h)
        let myView = UIView(frame: myRect)
        return myView
    }
    static  func getMonthOfDate (dateFormatter : DateFormatter )
    {
        
    }
    static func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let todayDate = Utils.stringToDate(date: today, formate: formatter.dateFormat)
        
        //        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
    static func getDayOfWeekName(numberOfDay : Int) -> String
    {
        switch numberOfDay {
        case 1:
            return "Sunday"
        case 2:
            return "Monday"
        case 3:
            return "Tuesday"
        case 4:
            return "Wednesday"
        case 5:
            return "Thursday"
        case 6:
            return "Friday"
        case 7:
            return "Saturday"
        default:
            Utils.customLogger(message: "Error fetching days", level: enums.printLevel.level1.rawValue)
            return "Day"
        }
    }
    static func getMonthName(numberOfMonth : Int) -> String
    {
        switch numberOfMonth {
        case 01:
            return "Jun"
        case 02:
            return "Feb"
        case 03:
            return "Mar"
        case 04:
            return "Apr"
        case 05:
            return "May"
        case 06:
            return "June"
        case 07:
            return "July"
        case 08:
            return "August"
        case 09:
            return "September"
        case 10:
            return "October"
        case 11:
            return "November"
        case 12:
            return "December"
        default:
            Utils.customLogger(message: "Error fetching days", level: enums.printLevel.level1.rawValue)
            return "Day"
        }
    }
    func heightForLabel(label: UILabel,text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    //    public static func MakeBoarderwithCustomHexColor(view : UIView,color : String){
    //        view.layer.cornerRadius = 5
    //        view.layer.borderWidth = 1
    //        view.layer.borderColor = color.cgColor
    //    }
    
    public static func MakeBoarderwithCustomColor(view : UIView,color : UIColor,cornerRadius : CGFloat,borderWidth : CGFloat){
        view.layer.cornerRadius = cornerRadius
        view.layer.borderWidth = borderWidth
        view.layer.borderColor = color.cgColor
    }
    public static func MakeBoarder(view : UIView){
        
        view.layer.cornerRadius = 5
        view.layer.borderWidth = 1
        view.layer.borderColor =  Utils.getUIColor(red: 35, green: 96, blue: 147,alpha:0.5).cgColor
        
    }
    
    static func addRowToStackView(_stackView : UIStackView, _addedView : UIView ,_height : CGFloat,_width: CGFloat){
        //        _addedView.heightAnchor.constraint(equalToConstant: _height).isActive = true
        //        _addedView.widthAnchor.constraint(equalToConstant: _width).isActive = true
        _stackView.addArrangedSubview(_addedView)
    }
    func isValidEmail(testStr:String) -> Bool {
        Utils.customLogger(message: "validate emilId: \(testStr)", level: enums.printLevel.level4.rawValue)
        
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    
    public static func extractTimeFromDate(stringDate : String,seperatorCharacter : String) -> String
    {
        let dateArr = stringDate.components(separatedBy: seperatorCharacter)
        let time = dateArr[1]
        return time
    }
    func validateEmail(enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
        
    }
//    func validatePhone(value: String) -> Bool {
//        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
//        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
//        let result =  phoneTest.evaluate(with: value)
//        return result
//    }
     func validatePhone(phoneNumber: String) -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = phoneNumber.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  phoneNumber == filtered
    }
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    
    func saveImageToDocumentDirectory(_ chosenImage: UIImage) -> String {
        
        var path : String = ""
        if let data = UIImagePNGRepresentation(chosenImage) {
            
            let date = Date()
            let formatter = DateFormatter()
            
            //        formatter.dateFormat = "dd.MM.yyyy"
            formatter.dateFormat = "dd/M/yyyyHmm"
            
            let filename = formatter.string(from: date).appending(".jpg")
            
            let filepath = getDocumentsDirectory().appendingPathComponent(filename)
            path = filename
            try? data.write(to: filepath)
            return path
        }
        return path
        
    }
    //used
    static func createDirectory(directoryName : String) -> String
    {
        var imagesDirectoryPath:String!
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        // Get the Document directory path
        let documentDirectorPath:String = paths[0]
        // Create a new path for the new images folder
        imagesDirectoryPath = documentDirectorPath + "/" + directoryName + "/"
        var objcBool:ObjCBool = true
        let isExist = FileManager.default.fileExists(atPath: imagesDirectoryPath, isDirectory: &objcBool)
        // If the folder with the given path doesn't exist already, create it
        if isExist == false{
            do{
                try FileManager.default.createDirectory(atPath: imagesDirectoryPath, withIntermediateDirectories: true, attributes: nil)
            }catch{
                Utils.customLogger(message:"Something went wrong while creating a new folder", level: enums.printLevel.level1.rawValue)
            }
        }
        return imagesDirectoryPath
        
    }
    
    
    static  func getDocumentsDirectory2() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    public static func saveIMG(chosenImage: UIImage , imageName : String)-> String{
        let documentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first
        let imageStore = documentsDirectory?.appendingPathComponent(imageName)
        let imageData = UIImagePNGRepresentation(chosenImage)
        
        do {
            try imageData?.write(to: imageStore!)
        } catch {
            Utils.customLogger(message:"Couldn't write the image to disk.", level: enums.printLevel.level1.rawValue)
        }
        
        return (imageStore?.absoluteString)!
        
    }
    
    
    public static  func getDirectoryPath5() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    
    
    public static func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    public static func removeOptionalFromString(string :String) -> String
    {
        var s = String(describing: string)
        s = s.components(separatedBy: "(").last!;
        s = s.replacingOccurrences(of: ")", with: "")
        s = s.replacingOccurrences(of: "\n", with: "")
        s = s.replacingOccurrences(of: "Optional", with: "")
        return s
    }
    
    public static func getUIColor(red : Float , green : Float, blue: Float,alpha: Float) -> UIColor
    {
        let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: CGFloat(1))
        return color
        
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
   
    
    /* ===============================================
     CONVERTING
     ===============================================
     */
    
    // Convert from JSON to nsdata
    func jsonToNSData(json: AnyObject) -> NSData?{
        do {
            return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData?
        } catch let myJSONError {
            Utils.customLogger(message:myJSONError as! String, level: enums.printLevel.level4.rawValue)
        }
        return nil;
    }
    static  func getFormattedDateAnswer(string: String, dateFormar : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +zzzz" // This formate is input formated .
        let formateDate = dateFormatter.date(from: string)!
        dateFormatter.dateFormat = dateFormar // Output Formated
        return dateFormatter.string(from: formateDate)
    }
    static  func getFormattedDate(string: String, dateFormar : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormar // Output Formated
        // This formate is input formated .
        let str : String = string;
        let formateDate = dateFormatter.date(from: str)
        return dateFormatter.string(from: formateDate!)
    }
    
    //CONVERT FROM NSDate to String
    
    public static func dateToString(date : Date,dateFormar : String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormar
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let dateString = dateFormatter.string(from:date as Date)
        return dateString
    }
    
    public func convertStringToDictionary(text: String) ->Any? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: [])
            } catch {
                Utils.customLogger(message:"error.localizedDescription", level: enums.printLevel.level4.rawValue)
            }
        }
        return nil
    }
    public  func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                Utils.customLogger(message:error.localizedDescription, level: enums.printLevel.level1.rawValue)
            }
        }
        return nil
    }
    public static  func extractDateFromUTCDate(dateUTCString : String) -> String {
        let string = dateUTCString
        
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: string)!
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    public static func circleImage(image : UIImageView,borderColor : CGColor){
        image.layer.borderWidth = 1
        image.layer.masksToBounds = true
        image.layer.borderColor = borderColor
        image.layer.cornerRadius = image.frame.height/2
        image.clipsToBounds = true
    }
   
    public static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    public static   func jsonToString(json: AnyObject) -> String{
        do {
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
            return convertedString!
            
        } catch let myJSONError {
            Utils.customLogger(message:myJSONError as! String, level: enums.printLevel.level4.rawValue)
        }
        return "";
        
    }
    public static func stringToDate(date:String , formate:String) -> Date
    {        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate //Your date format
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
        let date = dateFormatter.date(from: date) //according to date format your date string
        return date!
    }
    public static  func convertBase64ToUIImage(cadenaImagen: String) -> UIImage? {
        let encodedImageData = cadenaImagen
        let imageData = NSData(base64Encoded: encodedImageData)
        return UIImage(data: imageData! as Data) // Note it's optional. Don't force unwrap!!!
    }
    public static func seperateDateByDash(stringDate : String) -> String
    {
        
        let dateArr = stringDate.components(separatedBy: "-")
        
        let _dayNumber1    = dateArr[2].components(separatedBy: "T")
        let _dayNumber = _dayNumber1[0]
        let _year = dateArr[0]
        let month = dateArr[1]
        
        
        let date = _dayNumber + "-" + month + "-" + _year
        return date
        
        
    }
    
    func imageToBase64(img : UIImage) -> String {
        var imageData:NSData? = nil
        var imageDataStr:String? = nil
        
        
        let image : UIImage = img
        
        
        imageData = UIImagePNGRepresentation(image)! as NSData
        //encode
        let strBase64 = imageData?.base64EncodedString(options: .lineLength64Characters)
        imageDataStr = strBase64;
        return imageDataStr!
    }
    func saveDataInFile(fileName : String,fileTxt: String)
    {
        let file = fileName //this is the file. we will write to and read from it
        
        let text = fileTxt //just a text
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let path = dir.appendingPathComponent(file)
            
            //writing
            do {
                try text.write(to: path, atomically: false, encoding: String.Encoding.utf8)
            }
            catch {/* error handling here */}
            
            //reading
            do {
                try  _ = String(contentsOf: path, encoding: String.Encoding.utf8)
            }
            catch {/* error handling here */}
        }
    }
    static func getDateToday(formate: String) -> String
    {
        let date = Date()
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = formate
        let result = formatter.string(from: date)
        return result
        
    }
    static func getDateTimeNow (formate : String) -> String
    {
        let date = Date()
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = formate
        
        //        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let result = formatter.string(from: date)
        return result
    }
    func splitDateTime(date : String) -> String
    {
        let stripped = date.trimmingCharacters(in: .whitespacesAndNewlines)
        
        let split = stripped.substring(to: stripped.range(of: "T")?.lowerBound ?? (stripped.endIndex))
        var x = split.components(separatedBy: "T")
        return x[0]
    }
    
   
    
}





/* ===============================================
 extension
 ===============================================
 */
extension Array {
    func groupBy<G: Hashable>(groupClosure: (Element) -> G) -> [[Element]] {
        var groups = [[Element]]()
        
        for element in self {
            let key = groupClosure(element)
            var active = Int()
            var isNewGroup = true
            var array = [Element]()
            
            for (index, group) in groups.enumerated() {
                let firstKey = groupClosure(group[0])
                if firstKey == key {
                    array = group
                    active = index
                    isNewGroup = false
                    break
                }
            }
            
            array.append(element)
            
            if isNewGroup {
                groups.append(array)
            } else {
                groups.remove(at: active)
                groups.insert(array, at: active)
            }
        }
        
        return groups
    }
}
public extension Sequence {
    func group<U: Hashable>(by key: (Iterator.Element) -> U) -> [U:[Iterator.Element]] {
        var categories: [U: [Iterator.Element]] = [:]
        for element in self {
            let key = key(element)
            if case nil = categories[key]?.append(element) {
                categories[key] = [element]
            }
        }
        return categories
    }
}
/* ===============================================
 SDEFAULTS
 ===============================================
 */
struct sDefaults {
    let pref = UserDefaults()
    
    let userDetails = "DEFAULTS_USERDETAILS"
    let BaseUrl = "DEFAULTS_BASE_URL"
    
    func resetData()
    {
        pref.set(nil, forKey: userDetails)
        UserDefaults.standard.set(0, forKey: "LoggedIn")
        UserDefaults.standard.set(nil, forKey: "CheckedIn")
        UserDefaults.standard.set(nil, forKey: "UserRole")
        UserDefaults.standard.set(nil, forKey: "TokenType")
        UserDefaults.standard.set(0, forKey: "LoggedIn")
        UserDefaults.standard.set(nil, forKey: "UserToken")
        UserDefaults.standard.set(nil, forKey: "User_ID")
        UserDefaults.standard.set(nil, forKey: "UserLoginName")
        UserDefaults.standard.set(0, forKey: "UserRole")
        UserDefaults.standard.set(nil, forKey: "UserLoginPassword")
        UserDefaults.standard.set(nil, forKey: "CheckInDetail")
         UserDefaults.standard.set(nil, forKey: "ReportTypeId")
        UserDefaults.standard.set(nil, forKey: "SSMId")
        UserDefaults.standard.set(nil, forKey: "MesgText")
          UserDefaults.standard.set(nil, forKey: "ItemType")
        UserDefaults.standard.set(nil, forKey: "lastDate")
      UserDefaults.standard.set(nil, forKey: "Reportername")
        UserDefaults.standard.set(nil, forKey: "Page")
         UserDefaults.standard.set(nil, forKey: "DeviceToken")
    }
    
    func setTokenType(tokenType:String!) {
        UserDefaults.standard.set(tokenType, forKey: "TokenType")
    }
    func getTokenType() -> String! {
        if(isKeyPresentInUserDefaults(key: "TokenType")){
            return UserDefaults.standard.string(forKey: "TokenType")
        }
        return nil
    }
    func setDeviceToken(DeviceToken:String!) {
        UserDefaults.standard.set(DeviceToken, forKey: "DeviceToken")
    }
    func getDeviceToken() -> String! {
        if(isKeyPresentInUserDefaults(key: "DeviceToken")){
            return UserDefaults.standard.string(forKey: "DeviceToken")
        }
        return nil
    }
    func setDate() {
        let date = Date()
       
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let result = dateFormatter.string(from: date);        UserDefaults.standard.set(result, forKey: "lastDate")
    }
    func getDate() -> String! {
        self.setDate() 
        if(isKeyPresentInUserDefaults(key: "lastDate")){
            
            return UserDefaults.standard.string(forKey: "lastDate")
        }
        return nil
    }
    func setPage(Page: Int) {

       let pagenum = Page + 1
        UserDefaults.standard.set( pagenum ,forKey: "Page")
    }
    func getPage() -> Int! {

        if(isKeyPresentInUserDefaults(key: "Page")){

            return UserDefaults.standard.integer(forKey: "Page")
        }
        return nil
    }
//    func setDeviceLanguage(){
//
//        let locale = NSLocale.current.languageCode
//        UserDefaults.standard.set(locale, forKey: "DeviceLanguage")
//    }
//
//    func getDeviceLanguage() -> String! {
//        if(isKeyPresentInUserDefaults(key: "DeviceLanguage")){
//            return UserDefaults.standard.string(forKey: "DeviceLanguage")
//        }
//        return nil
//    }
    
    func setIsLoggedIn(isLoggedIn:Bool) {
        if isLoggedIn {
            UserDefaults.standard.set(1, forKey: "LoggedIn")
        } else {
            UserDefaults.standard.set(0, forKey: "LoggedIn")
        }
    }
    
    func getIsLoggedIn() -> Int!{
        if(isKeyPresentInUserDefaults(key: "LoggedIn")){
            return UserDefaults.standard.integer(forKey: "LoggedIn")
        }
        return nil
    }
    
    func setUserId(tID:String!) {
        UserDefaults.standard.set(tID, forKey: "User_ID")
    }
    func getUserId() -> String? {
        if(isKeyPresentInUserDefaults(key: "User_ID")){
            return UserDefaults.standard.string(forKey: "User_ID")
        }
        return nil
    }
    func setSSMId(SSMId:Int!) {
        UserDefaults.standard.set(SSMId, forKey: "SSMId")
    }
    func getSSMId() -> String! {
        if(isKeyPresentInUserDefaults(key: "SSMId")){
            return UserDefaults.standard.string(forKey: "SSMId")
        }
        return nil
    }
    func setItemType(ItemType:Int!) {
        UserDefaults.standard.set(ItemType, forKey: "ItemType")
    }
    func getItemType() -> Int! {
        if(isKeyPresentInUserDefaults(key: "ItemType")){
            return UserDefaults.standard.integer(forKey: "ItemType")
        }
        return nil
    }
    func setMesgText(MesgText:String!) {
        UserDefaults.standard.set(MesgText, forKey: "MesgText")
    }
    func getMesgText() -> String! {
        if(isKeyPresentInUserDefaults(key: "MesgText")){
            return UserDefaults.standard.string(forKey: "MesgText")
        }
        return nil
    }
    func setBaseUrl(IP:String!) {
        if IP != "" && IP  != nil{
            UserDefaults.standard.set(IP, forKey: "BaseUrl")
        }else{
            UserDefaults.standard.set("192.168.50.2:9494", forKey: "BaseUrl")
        }
    }
    func getBaseUrl() -> String? {
        if(isKeyPresentInUserDefaults(key: "BaseUrl")){
            return UserDefaults.standard.string(forKey: "BaseUrl")
        }
        return nil
    }
    func setUserLoginName(userName:String!) {
        UserDefaults.standard.set(userName, forKey: "UserLoginName")
    }
    func getUserLoginName() -> String? {
        if(isKeyPresentInUserDefaults(key: "UserLoginName")){
            return UserDefaults.standard.string(forKey: "UserLoginName")
        }
        return nil
    }
    func setUserLoginPass(userPass:String!) {
        UserDefaults.standard.set(userPass, forKey: "UserLoginPassword")
    }
    func getUserLoginPass() -> String? {
        if(isKeyPresentInUserDefaults(key: "UserLoginPassword")){
            return  UserDefaults.standard.string(forKey: "UserLoginPassword")
        }
        return nil
    }
    func setCurrentPrintLevel(level:Int!) {
        UserDefaults.standard.set(level, forKey: "PrintLevel")
    }
    
    func getCurrentPrintLevel() -> Int? {
        if(isKeyPresentInUserDefaults(key: "PrintLevel")){
            return  UserDefaults.standard.integer(forKey: "PrintLevel")
        }
        return nil
    }
    func setUserRole(role:Int!) {
        UserDefaults.standard.set(role, forKey: "UserRole")
    }
    
    func getUserRole() -> Int? {
        if(isKeyPresentInUserDefaults(key: "UserRole")){
            return  UserDefaults.standard.integer(forKey: "UserRole")
        }
        return nil
    }
    func setCheckInDetail(checkinObj:String){
        UserDefaults.standard.set(checkinObj, forKey: "CheckInDetail")
    }
    func getCheckInDetail() -> String?{
        if(isKeyPresentInUserDefaults(key: "CheckInDetail")){
            return UserDefaults.standard.string(forKey: "CheckInDetail")!
        }else{
            return nil
        }
    }
    func setReportTypeID(ReportTypeID:String){
        UserDefaults.standard.set(ReportTypeID, forKey: "ReportTypeID")
    }
    func getReportTypeID() -> String?{
        if(isKeyPresentInUserDefaults(key: "ReportTypeID")){
            return UserDefaults.standard.string(forKey: "ReportTypeID")!
        }else{
            return nil
        }
    }
    func setReportername(Reportername:String){
        UserDefaults.standard.set(Reportername, forKey: "Reportername")
    }
    func getReportername() -> String?{
        if(isKeyPresentInUserDefaults(key: "Reportername")){
            return UserDefaults.standard.string(forKey: "Reportername")!
        }else{
            return nil
        }
    }
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
}
extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }

}
