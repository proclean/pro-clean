//
//  URLConstants.swift
//  Unilever
//
//  Created by mac on 11/11/17.
//  Copyright © 2017 Smart tech. All rights reserved.
//

import Foundation
public struct APPURL {
    public struct Domains {
        static let Dev = "http://softgrow.net/dala"
        static let Local = "http://192.168.50.2:9234"
        static let Google = "http://35.195.252.229/"
    }

    public  struct Routes {
        static let UnileverApi = "/UnileverAPI/"
        static let systemCodeAPI = "/SystemCodeAPI/"
        static let userManagmentAPI = "/UserMngAPI/"
        }
}
