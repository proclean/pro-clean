//
//  CustomView.swift
//  Buffet
//
//  Created by Nerneen Mohamed on 5/8/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import Foundation
import UIKit
@IBDesignable
open class CustomView : UIView{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    @IBInspectable
    public var CornerRadius : CGFloat = 2.0 {
        didSet{
            self.layer.cornerRadius = self.CornerRadius
        }
    }
    @IBInspectable
    public var BorderWidth : CGFloat = 0.0 {
        didSet{
            self.layer.borderWidth = BorderWidth
        }
    }
    @IBInspectable
    public var BorderColor : UIColor = UIColor.clear {
        didSet{
            self.layer.borderColor = BorderColor.cgColor
        }
    }
}
