import Foundation
import UIKit
@IBDesignable
class CheckBox: UIButton {
    // Images
    //    cb_glossy_off
    //    cb_mono_off
    var checkedImage = UIImage(named: "cb_mono_on")! as UIImage
    var uncheckedImage = UIImage(named: "cb_mono_off")! as UIImage
    
    // Bool property
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, for: UIControlState.normal)
            } else {
                self.setImage(uncheckedImage, for: UIControlState.normal)
            }
        }
    }
    
    override func awakeFromNib() {
        self.addTarget(self, action:#selector(buttonClicked(sender:)), for: UIControlEvents.touchUpInside)
        self.isChecked = false
    }
    
    @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
        }
    }
    @IBInspectable
    public var ImageEdgeInsetsSpacing : CGFloat = 0.0 {
        didSet{
            self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, ImageEdgeInsetsSpacing);
        }
    }
    @IBInspectable
    public var TitleEdgeInsetsSpacing : CGFloat = 0.0 {
        didSet{
            self.titleEdgeInsets = UIEdgeInsetsMake(0, TitleEdgeInsetsSpacing, 0, 0);
        }
    }
    @IBInspectable
    public var ImageChecked : UIImage? = nil {
        didSet{
            self.checkedImage = ImageChecked!
        }
    }
    @IBInspectable
    public var ImageUnCheck : UIImage? = nil {
        didSet{
            self.uncheckedImage = ImageUnCheck!
        }
    }
}
