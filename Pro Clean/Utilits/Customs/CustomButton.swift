//
//  CustomButton.swift
//  Buffet
//
//  Created by Nerneen Mohamed on 5/7/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import Foundation
import UIKit
@IBDesignable
open class CustomButton : UIButton{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setTitle("My title", for: .normal)
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    @IBInspectable
    public var CornerRadius : CGFloat = 2.0 {
        didSet{
            self.layer.cornerRadius = self.CornerRadius
        }
    }
    @IBInspectable
    public var BorderWidth : CGFloat = 0.0 {
        didSet{
            self.layer.borderWidth = BorderWidth
        }
    }
    @IBInspectable
    public var BorderColor : UIColor = UIColor.clear {
        didSet{
            self.layer.borderColor = BorderColor.cgColor
        }
    }
    @IBInspectable
    public var UnderLineTextColor : UIColor = .clear {
        didSet{
            let yourAttributes : [NSAttributedStringKey: Any] = [
                NSAttributedStringKey.foregroundColor : self.currentTitleColor,
                NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue,
                NSAttributedStringKey.underlineColor : self.UnderLineTextColor]
            let attributeString = NSMutableAttributedString(string: (self.titleLabel?.text)!,
                                                            attributes: yourAttributes)
            self.setAttributedTitle(attributeString, for: .normal)
        }
    }
    @IBInspectable
    public var ImageEdgeInsetsSpacing : CGFloat = 0.0 {
        didSet{
            self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, ImageEdgeInsetsSpacing);
        }
    }
    @IBInspectable
    public var TitleEdgeInsetsSpacing : CGFloat = 0.0 {
        didSet{
            self.titleEdgeInsets = UIEdgeInsetsMake(0, TitleEdgeInsetsSpacing, 0, 0);
        }
    }
    @IBInspectable
    public var CircleButton : Bool = false {
        didSet{
            self.layer.borderWidth = 0
            self.layer.masksToBounds = false
          
            self.layer.cornerRadius = self.frame.height/2
            self.clipsToBounds = true
        }
    }
    
}
