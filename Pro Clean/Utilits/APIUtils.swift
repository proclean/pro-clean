//
//  APIHelperFuncs.swift
//  Unilever
//
//  Created by mac on 10/31/17.
//  Copyright © 2017 Smart tech. All rights reserved.
//


import Foundation
import UIKit
import ReachabilitySwift
class APIUtils
{
    public static func RequestEndDueToError(view : UIViewController,error:NSError)

    {
        IJProgressView.shared.hideProgressView();
         ViewControllerUtils.showAlert(view: view, title: NSLocalizedString("error", comment: ""), msg: error.localizedDescription){(result) in}
    }
     public static func unAuthorizationDialogue(view : UIViewController)
    {
        IJProgressView.shared.hideProgressView();


        ViewControllerUtils.showAlert(view: view, title: NSLocalizedString("unauthorized", comment: ""), msg: NSLocalizedString("unauthorizedMsg", comment: "")){(result) in
            if(result)
            {
//                let vc : LoginViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//
//                view.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }

    public static func alertMsg(view : UIViewController ,msg : String)
    {
        IJProgressView.shared.hideProgressView();

        ViewControllerUtils.showAlert(view: view, title: NSLocalizedString("message", comment: ""), msg: msg) { (result) in
        }
    }
    public static func reachabilityNotify(view : UIViewController,_selector : Selector)
    {
        DispatchQueue.main.async {

            let reachability = Reachability()!
            //declare this inside of viewWillAppear

            NotificationCenter.default.addObserver(view, selector: _selector,name: ReachabilityChangedNotification,object: reachability)
            do{
                try reachability.startNotifier()
            }catch{
                Utils.customLogger(message: "could not start reachability notifier", level: enums.printLevel.level4.rawValue)
            }
        }
    }

    public static func reachabilityChanged(view : UIViewController, note: Notification,onCompletion: @escaping (Bool) -> Void)
    {
        let reachability = note.object as! Reachability

        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                Utils.customLogger(message: "Reachable via WiFi", level: enums.printLevel.level4.rawValue)
            } else {
                Utils.customLogger(message: "Reachable via Cellular", level: enums.printLevel.level4.rawValue)
            }
        onCompletion(true)

        } else {
            Utils.customLogger(message: "Network not reachable", level: enums.printLevel.level4.rawValue)

            ViewControllerUtils.showDialogSingleBtn(view: view, title: "", msg: NSLocalizedString("noConnection", comment: "")){(data) in
            }
        }
    }
    
}
struct Headers {
    static let Authorization = "Authorization"
    static let ContentType = "Content-Type"
    static let Accept = "Accept"
}
