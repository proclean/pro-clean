//
//  RegisterationViewModel.swift
//  Buffet
//
//  Created by Nerneen Mohamed on 5/7/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import Foundation
import UIKit
class RegisterationViewModel :ViewModel{
    var fullname : String
    var email : String
    var password : String
    var phonenumber : String
    var phonenumberCode : String
    var city : String
    var userimagename : String
    var userimagetag : Int
    convenience init() {
        self.init(fullname: "", password: "", email: "", phonenumber: "", phonenumberCode: "", city: "", userimagetag: 0,userimagename:"")
    }
    init(fullname : String,password:String,email:String,phonenumber:String,phonenumberCode:String,city:String,userimagetag : Int,userimagename:String) {
        self.fullname = fullname
        self.password = password
        self.email = email
        self.phonenumber = phonenumber
        self.phonenumberCode = phonenumberCode
        self.city = city
        self.userimagetag = userimagetag
        self.userimagename = userimagename
    }
    var brokenRules: [BrokenRule] = [BrokenRule]()
    
    var isValid: Bool {
        get {
            self.brokenRules = [BrokenRule]()
            self.validate()
            return self.brokenRules.count == 0 ? true : false
        }
    }
    
}
extension RegisterationViewModel{
    private func validate(){
         let message = "Fill empty fields"
        if(userimagetag == 0){
            self.brokenRules.append(BrokenRule(propertyName: "userimage", messager: "avatar is Required"))
            return
        }
       
        if(fullname.isEmpty || fullname.replacingOccurrences(of: " ", with: "") == ""){
            self.brokenRules.append(BrokenRule(propertyName: "fullname", messager: message))
             return
        }
        if(password.isEmpty || password.replacingOccurrences(of: " ", with: "") == ""){
            self.brokenRules.append(BrokenRule(propertyName: "password", messager: message))
            return
        }
        if(email.isEmpty || email.replacingOccurrences(of: " ", with: "") == ""){
            self.brokenRules.append(BrokenRule(propertyName: "email", messager: message))
            return
        }
        if(phonenumber.isEmpty || phonenumber.replacingOccurrences(of: " ", with: "") == ""){
            self.brokenRules.append(BrokenRule(propertyName: "phonenumber", messager: message))
            return
        }
        if(city.isEmpty || city.replacingOccurrences(of: " ", with: "") == ""){
            self.brokenRules.append(BrokenRule(propertyName: "city", messager: message))
            return
        }
        if(!Utils().validateEmail(enteredEmail: self.email)){
            self.brokenRules.append(BrokenRule(propertyName: "emailFormateWrong", messager: NSLocalizedString("emailFormateWrong", comment: "")))
            return
        }
        if(!Utils().validatePhone(phoneNumber: self.phonenumber)){
            self.brokenRules.append(BrokenRule(propertyName: "phoneFormateWrong", messager: NSLocalizedString("phoneFormateWrong", comment: "")))
            return
        }
        
    }
    func getRegionsData(controller : RegisterationViewController){
        var lang = "en"
        if(UserDefaults().getDeviceLanguage().lowercased() == "ar"){
            lang = "ar"
        }
        let reference = DIDDataHandler()
        reference.getRegionsData(delegate: controller, lang: lang)
    }
}


