//
//  LoginViewModel.swift
//  Pro Clean
//
//  Created by Nerneen Mohamed on 5/15/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import Foundation
class LoginViewModel :ViewModel{
    var email : String
    var password : String
   
    convenience init() {
        self.init(password: "", email: "")
    }
    init(password:String,email:String) {
        self.password = password
        self.email = email
    }
    var brokenRules: [BrokenRule] = [BrokenRule]()

    var isValid: Bool {
        get {
            self.brokenRules = [BrokenRule]()
            self.validate()
            return self.brokenRules.count == 0 ? true : false
        }
    }
    
}
//MARK -> VALIDATION
extension LoginViewModel{
    private func validate(){
        if(email.isEmpty || email.replacingOccurrences(of: " ", with: "") == ""){
            self.brokenRules.append(BrokenRule(propertyName: "emptyFields", messager: NSLocalizedString("emptyFields", comment: "")))
            return
        }
        if(password.isEmpty || password.replacingOccurrences(of: " ", with: "") == ""){
            self.brokenRules.append(BrokenRule(propertyName: "emptyFields", messager: NSLocalizedString("emptyFields", comment: "")))
            return
        }
        if(!Utils().validateEmail(enteredEmail: self.email)){
            self.brokenRules.append(BrokenRule(propertyName: "emailFormateWrong", messager: NSLocalizedString("emailFormateWrong", comment: "")))
            return
        }
    }
    //MARK SAVEDATA
    func saveUserData(dict : [String:Any],onCompletion: @escaping (Bool) -> Void){
        let user = UserData.init(dictionary: dict["userData"] as! NSDictionary)
//        let user = UserModel.init(dictionary: dict as NSDictionary)
        UserDefaults().setIsLoggedIn(value: true)
        UserDefaults().setUserDetails(userObj: user!)
       // UserDefaults().setUserAddresses(userAddresses: user!.addresses!)
      //  UserDefaults().setUserToken(tID: user?.token)
        onCompletion(true)
    }
}




