//
//  ForgetPasswordViewModel.swift
//  Pro Clean
//
//  Created by Nerneen Mohamed on 5/18/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//


import Foundation
class ForgetPasswordViewModel :ViewModel{
    var email : String
    convenience init() {
        self.init(email: "")
    }
    init(email:String) {
        self.email = email
    }
    var brokenRules: [BrokenRule] = [BrokenRule]()
    
    var isValid: Bool {
        get {
            self.brokenRules = [BrokenRule]()
            self.validate()
            return self.brokenRules.count == 0 ? true : false
        }
    }
}
//MARK -> VALIDATION
extension ForgetPasswordViewModel{
    private func validate(){
        if(email.isEmpty || email.replacingOccurrences(of: " ", with: "") == ""){
            self.brokenRules.append(BrokenRule(propertyName: "emptyFields", messager: NSLocalizedString("emptyFields", comment: "")))
            return
        }
       
        if(!Utils().validateEmail(enteredEmail: self.email)){
            self.brokenRules.append(BrokenRule(propertyName: "emailFormateWrong", messager: NSLocalizedString("emailFormateWrong", comment: "")))
            return
        }
    }
}




