//
//  ChangePasswordViewModel.swift
//  Pro Clean
//
//  Created by Nerneen Mohamed on 5/18/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import Foundation
class ChangePasswordViewModel :ViewModel{
    var oldPassword : String
    var newPassword : String
    var renewPassword : String
    convenience init() {
        self.init(oldPassword: "",newPassword:"",renewPassword:"")
    }
    init(oldPassword:String,newPassword:String,renewPassword:String) {
        self.oldPassword = oldPassword
        self.newPassword = newPassword
        self.renewPassword = renewPassword
    }
    var brokenRules: [BrokenRule] = [BrokenRule]()
    
    var isValid: Bool {
        get {
            self.brokenRules = [BrokenRule]()
            self.validate()
            return self.brokenRules.count == 0 ? true : false
        }
    }
}
//MARK -> VALIDATION
extension ChangePasswordViewModel{
    private func validate(){
        if(oldPassword.isEmpty || oldPassword.replacingOccurrences(of: " ", with: "") == "" || newPassword.isEmpty || newPassword.replacingOccurrences(of: " ", with: "") == "" || renewPassword.isEmpty || renewPassword.replacingOccurrences(of: " ", with: "") == "" ){
            self.brokenRules.append(BrokenRule(propertyName: "emptyFields", messager: NSLocalizedString("emptyFields", comment: "")))
            return
        }
        if(newPassword != renewPassword){
            self.brokenRules.append(BrokenRule(propertyName: "confirmError", messager: NSLocalizedString("confirmError", comment: "")))
        }
        
    }
}
