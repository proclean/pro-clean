//
//  sideMenuTableViewCell.swift
//  Pro Clean
//
//  Created by Nerneen Mohamed on 5/26/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import UIKit

class sideMenuTableViewCell: UITableViewCell {
    @IBOutlet weak var imgSideMenu: UIImageView!
    
    @IBOutlet weak var titleSideMenu: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
