//
//  LoginSideMenuTableViewCell.swift
//  Pro Clean
//
//  Created by Nerneen Mohamed on 5/26/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import UIKit

class LoginSideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var imgUser: CustomButton!
    @IBOutlet weak var lblUserBalance: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configureCell(){
        let isLogin = UserDefaults().isLoggedIn()
        if isLogin{
            self.imgUser.setTitle("", for: .normal)
            let user = UserDefaults().getUserDetails()
           self.lblUserName.text =  user?.full_name ?? ""
            self.lblUserBalance.text = (user?.balance)! + "  KWD"
            self.imgUser.setImage(UIImage(named: (user?.image)!), for: .normal)
            self.lblUserName.font = UIFont.systemFont(ofSize: 16)
            

        }else{
            self.lblUserName.text = "Login"
            self.lblUserName.font = UIFont.boldSystemFont(ofSize: 20)
        }
        self.imgUser.CircleButton = isLogin
        self.imgUser.isHidden = !isLogin
        self.lblUserBalance.isHidden = !isLogin
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
