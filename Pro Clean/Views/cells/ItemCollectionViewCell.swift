//
//  ItemCollectionViewCell.swift
//  Pro Clean
//
//  Created by Nerneen Mohamed on 5/19/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import UIKit
var imageCache = NSCache<AnyObject, AnyObject>()

class ItemCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblItem: UILabel!
    @IBOutlet weak var imgItem: UIImageView!
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    var item : ItemsData?{
        didSet{
            lblItem.text = item?.title
            imgItem.image = nil
            activityIndicator.center = CGPoint(x: imgItem.bounds.size.width/2, y: imgItem.bounds.size.height/2)
            self.imgItem.addSubview(activityIndicator)
            if let statusImageUrl = item?.icon{
                if let image = imageCache.object(forKey: statusImageUrl as AnyObject) as? UIImage{
                    imgItem.image = image
                }else{
                    let Url = String(format: statusImageUrl)
                    guard let serviceUrl = URL(string: Url) else { return }
                    activityIndicator.startAnimating()
                    URLSession.shared.dataTask(with: serviceUrl, completionHandler: { (data, response, error) -> Void in
                        if error != nil{
                            print(error)
                            return
                        }
                        let image = UIImage(data:data!)
                        imageCache.setObject(image!, forKey: statusImageUrl as AnyObject)
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                            self.imgItem.image = image
                        }
                    }).resume()
                }
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
