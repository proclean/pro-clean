//
//  APIDelegate
//  SmartPieces
//
//  Created by Smart Tech on 8/7/17.
//  Copyright © 2017 Smart tech. All rights reserved.
//

import Foundation

@objc protocol APIDelegate: NSObjectProtocol{
    
    func RequestEndDueToError(error:NSError)
    @objc optional func setusetData(model:[String:Any])
    @objc optional func registerationData(model:[String:Any])
    @objc optional func packages(model:[[String:Any]])
    @objc optional func getPriceList(model:[[String:Any]])
    @objc optional func getOrderList(model:[[String:Any]])
    @objc optional func signOut(model:String)
    @objc optional func changePassword(model:String)
    @objc optional func getRegionsData(model:[[String:Any]])
    @objc optional func getSlidesData(model:[[String:Any]])


    


    
    @objc optional func serviceData(arrayData:NSArray)
    
    @objc optional func serviceData(model:Any)
    
    @objc optional func serviceDataSchedule(model:Any)
    
    @objc optional func alertMsg(msg : String)
    
    @objc optional func unAuthorizationDialogue()
    
    @objc optional func checkInResult(model:[String:Any])
    @objc optional func checkOutResult(model:[String:Any])
    
    //list agent product
    @objc optional func agentProductListByLocation(model:[[String:Any]])
    
    //change password
    @objc optional func endChangePassword(model:Bool)
    //add agentSale
    @objc optional func addAgentSale(model:[String:Any])
    // agent daily sales
    @objc optional func agentDailySales(model:[String:Any])
    //lst status for check in out
    @objc optional func getAgentLastStatus(model:[String:Any])
    //Manager API
    @objc optional func getAgentsOnMap(model:[[String:Any]])
    
    @objc optional func getProductList(model:[[String:Any]])
    
    @objc optional func locationList(model :[[String:Any]])
    @objc optional func getCategoryList(model:[String:Any])
    @objc optional func getProductListByCategoryId(model:[String:Any])
    @objc optional func getReportTypes(model:[String:Any])
    @objc optional func ReportType(model:[String:Any])
    @objc optional func addMessageResult(model:[String:Any])
    @objc optional func ManagerOpenChat(model:[String:Any])
    @objc optional func AgentMessage(model:[String:Any])
    @objc optional func ManagerMessage(model:[String:Any])
    @objc optional func RegisterTokenResult(model:[String:Any])
}
