//
//  DIDataHandler.swift
//  Unilever
//
//  Created by SmartTech on 8/7/17.
//  Copyright © 2017 SmartTech. All rights reserved.
//

import Foundation
import Alamofire


public class DIDDataHandler:NSObject
{
    
    var apiHelper = APIHelper()
    
    static let sharedInstance = DIDDataHandler()
    
    //MARK:- LOGIN
    func login(delegate:APIDelegate,email : String , _password: String){
        self.apiHelper.delegate=delegate;
        self.apiHelper.login(email, password: _password)
    }
    func signup(delegate:APIDelegate,model:[String:Any]){
        self.apiHelper.delegate=delegate;
        self.apiHelper.signUp(model: model)
    }
    func getPackageList(delegate:APIDelegate){
        self.apiHelper.delegate=delegate;
        self.apiHelper.getPackageList()
    }
    func getPriceList(delegate:APIDelegate){
        self.apiHelper.delegate=delegate;
        self.apiHelper.getPriceList()
    }
    func getOrderList(delegate:APIDelegate,userid:String,token:String,status:String){
        self.apiHelper.delegate=delegate;
        self.apiHelper.getOrderList(userId: userid, token: token, status: status)
    }
    func signOut(delegate:APIDelegate,userId : String,token : String){
        self.apiHelper.delegate=delegate;
        self.apiHelper.signOut(userId: userId, token: token)
    }
    func changePassword(delegate:APIDelegate,model:[String:Any]){
        self.apiHelper.delegate=delegate;
        self.apiHelper.changePassword(model: model)
    }
    func getRegionsData(delegate:APIDelegate,lang:String){
        self.apiHelper.delegate=delegate;
        self.apiHelper.getRegionsData(lang: lang)
    }
    func getSliderImages(delegate:APIDelegate){
        self.apiHelper.delegate=delegate;
        self.apiHelper.getSliderImages()
    }
}
