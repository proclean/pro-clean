//
//  APIHelper.swift
//  iTeacherApp
//
//  Created by Smart Tech on 3/12/17.
//  Copyright © 2017 Smart Tech. All rights reserved.
//
import Foundation
import Alamofire
import SwiftyJSON
class APIHelper :NSObject {
    weak var delegate: APIDelegate?
    private  static let Domain = APPURL.Domains.Dev
    private  static let BaseURL = Domain
    //MARK -> APP URLS
    enum API: String {
        case login = "/clients/login"
        case signUp = "/clients/sign_up"
        case getPackagesList = "/packages/get_packageslist"
        case getOrderList = "/orders/get_orderlist?userid="
        case getPriceList = "/en/items/get_itemslist"
        case signOut = "/delegates/logout"
        case changePassword = "/clients/change_password"
        case getRegionData = "/regions/get_regions "
    }
    func login(_ email: String, password:String){
        
        let headers: HTTPHeaders = [
            Headers.ContentType : "application/json"
        ]
        let todosEndpoint: String = APIHelper.BaseURL +  API.login.rawValue;
        
        let newTodo = ["email":email, "password": password] as [String : Any]
        
        Alamofire.request(todosEndpoint , method: .post, parameters: newTodo,encoding: JSONEncoding.default, headers: headers )
            .responseJSON { response in
                switch(response.result)
                {
                case .success(_):
                    if let dict = response.result.value as? [String : Any]{
                        let exist = dict["result"] != nil
                        if(exist){
                            let result = dict["result"] as! String
                            if(result != "Success"){
                                self.delegate?.alertMsg!(msg: dict["msg"] as! String)
                                break;
                            }else{

                                
                                self.delegate?.setusetData!(model: dict);                                break;
                            }
                        }
                    }
                    else
                    {
                        self.delegate?.RequestEndDueToError(error: response.result.error! as NSError)
                        break;
                    }

                   
                case .failure(_):
                    self.delegate?.RequestEndDueToError(error: response.result.error! as NSError)
                    break
                }
        }
    }
    func signUp(model:[String:Any]){
        
        let headers: HTTPHeaders = [
            Headers.ContentType : "application/json"
        ]
        
        let todosEndpoint: String = APIHelper.BaseURL +  API.signUp.rawValue;
        
        let newTodo =  model
        
        Alamofire.request(todosEndpoint , method: .post, parameters: newTodo,encoding: JSONEncoding.default, headers: headers )
            .responseJSON { response in
                switch(response.result)
                {
                case .success(_):
                    if let dict = response.result.value as? [String : Any]{
                        let exist = dict["result"] != nil
                        if(exist){
                            let result = dict["result"] as! String
                            if(result != "Success"){
                                self.delegate?.alertMsg!(msg: dict["msg"] as! String)
                                break;
                            }else{
                            
                                self.delegate?.registerationData!(model: dict);                                break;
                            }
                        }
                    }
                    else
                    {
                        self.delegate?.RequestEndDueToError(error: response.result.error! as NSError)
                        break;
                    }
                    
                    
                case .failure(_):
                    self.delegate?.RequestEndDueToError(error: response.result.error! as NSError)
                    break
                }
        }
    }
    
    
    func getPackageList(){
        let todosEndpoint: String = APIHelper.BaseURL +  API.getPackagesList.rawValue;
        Alamofire.request(todosEndpoint).responseJSON {response in
            switch(response.result){
            case .success(_):
                if let dict = response.result.value as? [String : Any]{
                    let exist = dict["result"] != nil
                    if(exist){
                        let result = dict["result"] as! String
                        if(result != "Success"){
                            self.delegate?.alertMsg!(msg: dict["msg"] as! String)
                            break;
                        }else{
                            self.delegate?.packages!(model: dict["packagesData"] as! [[String:Any]])
                            break;
                        }
                    }
                }
                else{
                    self.delegate?.RequestEndDueToError(error: response.result.error! as NSError)
                    break;
                }
            case .failure(_):
                self.delegate?.RequestEndDueToError(error: response.result.error! as NSError)
                break
            }
        }
    }
    
    
    func getPriceList(){
        let todosEndpoint: String = APIHelper.BaseURL +  API.getPriceList.rawValue;
        Alamofire.request(todosEndpoint).responseJSON {response in
            switch(response.result){
            case .success(_):
                if let dict = response.result.value as? [String : Any]{
                    let exist = dict["result"] != nil
                    if(exist){
                        let result = dict["result"] as! String
                        if(result != "Success"){
                            self.delegate?.alertMsg!(msg: dict["msg"] as! String)
                            break;
                        }else{
                           
                            self.delegate?.getPriceList!(model: dict["itemsData"] as! [[String:Any]])
                            break;
                        }
                    }
                }
                else{
                    self.delegate?.RequestEndDueToError(error: response.result.error! as NSError)
                    break;
                }
            case .failure(_):
                self.delegate?.RequestEndDueToError(error: response.result.error! as NSError)
                break
            }
        }
    }
    func getOrderList(userId:String,token:String,status:String){
        let todosEndpoint: String = APIHelper.BaseURL +  API.getOrderList.rawValue + userId + "&usertype=client&token=" + token + "&offset=0&status=" + status

        Alamofire.request(todosEndpoint).responseJSON {response in
            switch(response.result){
            case .success(_):
                if let dict = response.result.value as? [String : Any]{
                    let exist = dict["result"] != nil
                    if(exist){
                        let result = dict["result"] as! String
                        if(result != "Success"){
                            self.delegate?.alertMsg!(msg: dict["msg"] as! String)
                            break;
                        }else{
                            
                            self.delegate?.getOrderList!(model: dict["orderData"] as! [[String:Any]])
                            break;
                        }
                    }
                }
                else{
                    self.delegate?.RequestEndDueToError(error: response.result.error! as NSError)
                    break;
                }
            case .failure(_):
                self.delegate?.RequestEndDueToError(error: response.result.error! as NSError)
                break
            }
        }
    }
    func signOut(userId : String,token : String){
        
        let headers: HTTPHeaders = [
            Headers.ContentType : "application/json"
        ]
        
        let todosEndpoint: String = APIHelper.BaseURL +  API.signOut.rawValue;
        let parameters = [
            "userID":userId,
            "token": token
        ]
        let newTodo =  parameters
        
        Alamofire.request(todosEndpoint , method: .post, parameters: newTodo,encoding: JSONEncoding.default, headers: headers )
            .responseJSON { response in
                switch(response.result)
                {
                case .success(_):
                    if let dict = response.result.value as? [String : Any]{
                        let exist = dict["result"] != nil
                        if(exist){
                            let result = dict["result"] as! String
                            if(result != "Success"){
                                self.delegate?.alertMsg!(msg: dict["msg"] as! String)
                                break;
                            }else{
                                self.delegate?.signOut!(model: dict["msg"] as! String)
                            }
                        }
                    }
                    else
                    {
                        self.delegate?.RequestEndDueToError(error: response.result.error! as NSError)
                        break;
                    }
                    
                    
                case .failure(_):
                    self.delegate?.RequestEndDueToError(error: response.result.error! as NSError)
                    break
                }
        }
    }
    func changePassword(model:[String:Any]){
        
        let headers: HTTPHeaders = [
            Headers.ContentType : "application/json"
        ]
        
        let todosEndpoint: String = APIHelper.BaseURL +  API.changePassword.rawValue;
        let parameters = [
            
            "current_password":model["current_password"],
            "password":model["password"],
            "confirm_password":model["confirm_password"],
            "userID":model["userID"],
            "token":model["token"]
            
            
        ]
        let newTodo =  parameters
        
        Alamofire.request(todosEndpoint , method: .post, parameters: newTodo,encoding: JSONEncoding.default, headers: headers )
            .responseJSON { response in
                switch(response.result)
                {
                case .success(_):
                    if let dict = response.result.value as? [String : Any]{
                        let exist = dict["result"] != nil
                        if(exist){
                            let result = dict["result"] as! String
                            if(result != "Success"){
                                self.delegate?.alertMsg!(msg: dict["msg"] as! String)
                                break;
                            }else{
                                self.delegate?.changePassword!(model: dict["msg"] as! String)
                            }
                        }
                    }
                    else
                    {
                        self.delegate?.RequestEndDueToError(error: response.result.error! as NSError)
                        break;
                    }
                    
                    
                case .failure(_):
                    self.delegate?.RequestEndDueToError(error: response.result.error! as NSError)
                    break
                }
        }
    }
    func getRegionsData(lang : String){
        var todosEndpoint: String = APIHelper.BaseURL + "/" + lang  + API.getRegionData.rawValue;
        todosEndpoint = "http://softgrow.net/dala/en/regions/get_regions"
        Alamofire.request(todosEndpoint).responseJSON {response in
            switch(response.result){
            case .success(_):
                if let dict = response.result.value as? [String : Any]{
                    let exist = dict["result"] != nil
                    if(exist){
                        let result = dict["result"] as! String
                        if(result != "Success"){
                            self.delegate?.alertMsg!(msg: dict["msg"] as! String)
                            break;
                        }else{
                            self.delegate?.getRegionsData!(model: dict["regionsData"] as! [[String:Any]])
                            break;
                        }
                    }
                }
                else{
                    self.delegate?.RequestEndDueToError(error: response.result.error! as NSError)
                    break;
                }
            case .failure(_):
                self.delegate?.RequestEndDueToError(error: response.result.error! as NSError)
                break
            }
        }
    }
    func getSliderImages(){
        let todosEndpoint: String = "http://softgrow.net/dala/slideshow/get_sliderslist"
        Alamofire.request(todosEndpoint).responseJSON {response in
            switch(response.result){
            case .success(_):
                if let dict = response.result.value as? [String : Any]{
                    let exist = dict["result"] != nil
                    if(exist){
                        let result = dict["result"] as! String
                        if(result != "Success"){
                            self.delegate?.alertMsg!(msg: dict["msg"] as! String)
                            break;
                        }else{
                            self.delegate?.getSlidesData!(model: dict["slidesData"] as! [[String:Any]])
                            break;
                        }
                    }
                }
                else{
                    self.delegate?.RequestEndDueToError(error: response.result.error! as NSError)
                    break;
                }
            case .failure(_):
                self.delegate?.RequestEndDueToError(error: response.result.error! as NSError)
                break
            }
        }
    }
    
}
