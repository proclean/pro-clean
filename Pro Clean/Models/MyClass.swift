//
//  MyClass.swift
//  Pro Clean
//
//  Created by Maged on 4/5/18.
//  Copyright © 2018 Mego. All rights reserved.
//

import UIKit

class MyClass: NSObject {
    class myButton : UIButton {
        override var isHighlighted: Bool {
            didSet {
                if (isHighlighted) {
                    self.backgroundColor = UIColor.blue
                } else {
                    self.backgroundColor = UIColor.white
                }
            }
        }
    }
  
    
}
