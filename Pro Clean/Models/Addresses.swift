import Foundation

 class Addresses: NSObject,NSCoding {
	public var iD : Int?
	public var clientID : Int?
	public var name : String?
	public var address : String?
	public var lat : Double?
	public var lng : Double?

    public class func modelsFromDictionaryArray(array:NSArray) -> [Addresses]
    {
        var models:[Addresses] = []
        for item in array
        {
            models.append(Addresses(dictionary: item as! NSDictionary)!)
        }
        return models
    }


	required public init?(dictionary: NSDictionary) {

		iD = dictionary["ID"] as? Int
		clientID = dictionary["clientID"] as? Int
		name = dictionary["name"] as? String
		address = dictionary["address"] as? String
		lat = dictionary["lat"] as? Double
		lng = dictionary["lng"] as? Double
	}


	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.iD, forKey: "ID")
		dictionary.setValue(self.clientID, forKey: "clientID")
		dictionary.setValue(self.name, forKey: "name")
		dictionary.setValue(self.address, forKey: "address")
		dictionary.setValue(self.lat, forKey: "lat")
		dictionary.setValue(self.lng, forKey: "lng")

		return dictionary
	}
    
    // MARK: NSCoding
   
    init(iD:Int? = 0
        ,clientID:Int? = 0,name:String? = "",address:String? = "",lat:Double? = 0,lng:Double? = 0) {
        self.iD = iD
        self.clientID = clientID
        self.name=name
        self.address=address
        self.lat = lat
        self.lng = lng
    }
    
    
    required convenience init?(coder decoder: NSCoder) {
        
        guard let iD = decoder.decodeObject(forKey: "iD") as? Int,
            let clientID = decoder.decodeObject(forKey: "clientID") as? Int,
            let name = decoder.decodeObject(forKey: "name") as? String,
            let address = decoder.decodeObject(forKey: "address") as? String,
            let lat = decoder.decodeObject(forKey: "lat") as? Double,
            let lng = decoder.decodeObject(forKey: "lng") as? Double
            else { return nil }
        self.init(iD: iD, clientID: clientID, name: name, address: address, lat: lat, lng: lng)
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.iD, forKey: "iD")
        aCoder.encode(self.clientID, forKey: "clientID")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.address, forKey: "address")
        aCoder.encode(self.lat, forKey: "lat")
        aCoder.encode(self.lng, forKey: "lng")
    }
}
