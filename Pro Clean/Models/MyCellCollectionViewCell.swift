//
//  MyCellCollectionViewCell.swift
//  Pro Clean
//
//  Created by Maged on 4/18/18.
//  Copyright © 2018 Mego. All rights reserved.
//

import UIKit

class MyCellCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cost: UILabel!
    @IBOutlet weak var point: UILabel!
    @IBOutlet weak var disc: UILabel!
    @IBOutlet weak var tit: UILabel!
    @IBOutlet weak var image: UIImageView!
}
