//
//  UserModel.swift
//  Pro Clean
//
//  Created by Nermeen Mohamed on 5/12/18.
//  Copyright © 2018 Mego. All rights reserved.
//

import Foundation
/*
 AGENT DETAILS MODEL
 */
class UserModel: NSObject,NSCoding {

    public var id : Int?
    public var full_name : String?
    public var email : String?
    public var password : String?
    public var mobile : String?
    public var regionId : String?
    public var image : String?
    public var firebaseToken : String?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let obj = UserModel.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of UserModel Instances.
     */
    
    public class func populateUsers(array:NSArray) -> [UserModel]{
        var models:[UserModel] = []
        for item in array{
            models.append(UserModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let obj = UserModel(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: UserModel Instance.
     */
    public init?(dictionary: NSDictionary) {
        id = dictionary["Id"] as? Int
        full_name = dictionary["full_name"] as? String
        email = dictionary["email"] as? String
        password = dictionary["password"] as? String
        mobile = dictionary["mobile"] as? String
        regionId = dictionary["regionID"] as? String
        image = dictionary["image"] as? String
        firebaseToken = dictionary["firebaseToken"] as? String
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.id, forKey: "Id")
        dictionary.setValue(self.full_name, forKey: "full_name")
        dictionary.setValue(self.email, forKey: "email")
        dictionary.setValue(self.mobile, forKey: "mobile")
        dictionary.setValue(self.password, forKey: "password")
        dictionary.setValue(self.regionId, forKey: "regionID")
        dictionary.setValue(self.image, forKey: "image")
        dictionary.setValue(self.firebaseToken, forKey: "firebaseToken")
        return dictionary
    }
    
    
    // MARK: NSCoding
    init(full_name:String? = ""
        ,password:String? = "",regionId:String? = "",image:String? = "",email:String? = "",firebaseToken:String? = "",mobile:String? = "") {
        self.full_name=full_name
        self.password = password
        self.email=email
        self.mobile=mobile
        self.regionId = regionId
        self.image = image
        self.firebaseToken = firebaseToken
    }
    
    
    required convenience init?(coder decoder: NSCoder) {
        guard let full_name = decoder.decodeObject(forKey: "full_name") as? String,
            let password = decoder.decodeObject(forKey: "password") as? String,
            let email = decoder.decodeObject(forKey: "email") as? String,
            let mobile = decoder.decodeObject(forKey: "mobile") as? String,
            let regionId = decoder.decodeObject(forKey: "regionID") as? String,
            let image = decoder.decodeObject(forKey: "image") as? String,
            let firebaseToken = decoder.decodeObject(forKey: "firebaseToken") as? String
            else { return nil }
        self.init(full_name: full_name, password: password, regionId: regionId, image: image, email: email, firebaseToken: firebaseToken, mobile: mobile)
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.full_name, forKey: "full_name")
        aCoder.encode(self.password, forKey: "password")
        aCoder.encode(self.email, forKey: "email")
        aCoder.encode(self.mobile, forKey: "mobile")
        aCoder.encode(self.regionId, forKey: "regionID")
        aCoder.encode(self.image, forKey: "image")
        aCoder.encode(self.firebaseToken, forKey: "firebaseToken")
    }
}
