//
//  MsgCellTableViewCell.swift
//  Pro Clean
//
//  Created by Maged on 5/9/18.
//  Copyright © 2018 Mego. All rights reserved.
//

import UIKit

class MsgCellTableViewCell: UITableViewCell {

    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var Name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
