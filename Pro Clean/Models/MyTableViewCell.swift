//
//  MyTableViewCell.swift
//  Pro Clean
//
//  Created by Maged on 4/29/18.
//  Copyright © 2018 Mego. All rights reserved.
//

import UIKit

class MyTableViewCell: UITableViewCell {

  
    @IBOutlet var title: UILabel!
    
    @IBOutlet var myimage: UIImageView!
    @IBOutlet var price1: UILabel!
    @IBOutlet var price2: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
