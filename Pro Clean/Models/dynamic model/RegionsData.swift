import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class RegionsData {
	public var iD : String?
	public var provinceID : String?
	public var name : String?
	public var province_name : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let regionsData_list = RegionsData.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of RegionsData Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [RegionsData]
    {
        var models:[RegionsData] = []
        for item in array
        {
            models.append(RegionsData(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let regionsData = RegionsData(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: RegionsData Instance.
*/
	required public init?(dictionary: NSDictionary) {

		iD = dictionary["ID"] as? String
		provinceID = dictionary["provinceID"] as? String
		name = dictionary["name"] as? String
		province_name = dictionary["province_name"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.iD, forKey: "ID")
		dictionary.setValue(self.provinceID, forKey: "provinceID")
		dictionary.setValue(self.name, forKey: "name")
		dictionary.setValue(self.province_name, forKey: "province_name")

		return dictionary
	}

}
