import Foundation
class UserData :NSObject,NSCoding{
	public var iD : Int?
	public var full_name : String?
	public var regionID : String?
	public var email : String?
	public var mobile : String?
	public var image : String?
	public var address : String?
	public var lat : String?
	public var lng : String?
	public var balance : String?
	public var token : String?
	public var userType : String?
	public var activation_code : String?
	public var promo_code : String?
	public var total_purchases : String?
	public var verified : String?
	public var createdOn : String?
	public var addresses : Array<Addresses>?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let userData_list = UserData.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of UserData Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [UserData]
    {
        var models:[UserData] = []
        for item in array
        {
            models.append(UserData(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let userData = UserData(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: UserData Instance.
*/
	required public init?(dictionary: NSDictionary) {

		iD = dictionary["ID"] as? Int
		full_name = dictionary["full_name"] as? String
		regionID = dictionary["regionID"] as? String
		email = dictionary["email"] as? String
		mobile = dictionary["mobile"] as? String
		image = dictionary["image"] as? String
		address = dictionary["address"] as? String
		lat = dictionary["lat"] as? String
		lng = dictionary["lng"] as? String
		balance = dictionary["balance"] as? String
		token = dictionary["token"] as? String
		userType = dictionary["userType"] as? String
		activation_code = dictionary["activation_code"] as? String
		promo_code = dictionary["promo_code"] as? String
		total_purchases = dictionary["total_purchases"] as? String
		verified = dictionary["verified"] as? String
		createdOn = dictionary["createdOn"] as? String
        if (dictionary["addresses"] != nil) { addresses = Addresses.modelsFromDictionaryArray(array: dictionary["addresses"] as! NSArray) }
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.iD, forKey: "ID")
		dictionary.setValue(self.full_name, forKey: "full_name")
		dictionary.setValue(self.regionID, forKey: "regionID")
		dictionary.setValue(self.email, forKey: "email")
		dictionary.setValue(self.mobile, forKey: "mobile")
		dictionary.setValue(self.image, forKey: "image")
		dictionary.setValue(self.address, forKey: "address")
		dictionary.setValue(self.lat, forKey: "lat")
		dictionary.setValue(self.lng, forKey: "lng")
		dictionary.setValue(self.balance, forKey: "balance")
		dictionary.setValue(self.token, forKey: "token")
		dictionary.setValue(self.userType, forKey: "userType")
		dictionary.setValue(self.activation_code, forKey: "activation_code")
		dictionary.setValue(self.promo_code, forKey: "promo_code")
		dictionary.setValue(self.total_purchases, forKey: "total_purchases")
		dictionary.setValue(self.verified, forKey: "verified")
		dictionary.setValue(self.createdOn, forKey: "createdOn")

		return dictionary
	}
    // MARK: NSCoding
    
    init(iD:Int? = 0
        ,full_name:String? = "",regionID:String? = "",email:String? = "",mobile:String? = "",image:String? = "",address:String = "",lat:String = "",lng:String = "",balance:String = "",token:String = "",userType:String = "",activation_code:String = "",promo_code:String = "",total_purchases:String = "",verified:String = "",createdOn:String = "") {
        self.iD = iD
        self.full_name = full_name
        self.regionID = regionID
        self.email = email
        self.mobile = mobile
        self.image = image
        self.address = address
        self.lat = lat
        self.lng = lng
        self.balance = balance
        self.token = token
        self.userType = userType
        self.activation_code = activation_code
        self.verified = verified
        self.createdOn = createdOn
    }
    
    
    required convenience init?(coder decoder: NSCoder) {
        
        guard let iD = decoder.decodeObject(forKey: "iD") as? Int,
            let full_name = decoder.decodeObject(forKey: "full_name") as? String,
            let regionID = decoder.decodeObject(forKey: "regionID") as? String,
            let email = decoder.decodeObject(forKey: "email") as? String,
            let mobile = decoder.decodeObject(forKey: "mobile") as? String,
            let image = decoder.decodeObject(forKey: "image") as? String,
       // let address = decoder.decodeObject(forKey: "address") as? String,
        let lng = decoder.decodeObject(forKey: "lng") as? String,
        let lat = decoder.decodeObject(forKey: "lat") as? String,
            let balance = decoder.decodeObject(forKey: "balance") as? String,
        let token = decoder.decodeObject(forKey: "token") as? String,
        let userType = decoder.decodeObject(forKey: "userType") as? String,
        let activation_code = decoder.decodeObject(forKey: "activation_code") as? String,
            let promo_code = decoder.decodeObject(forKey: "promo_code") as? String,
        let total_purchases = decoder.decodeObject(forKey: "total_purchases") as? String,
        let verified = decoder.decodeObject(forKey: "verified") as? String,
        let createdOn = decoder.decodeObject(forKey: "createdOn") as? String
            else { return nil }
        
        self.init(iD: iD, full_name: full_name, regionID: regionID, email: email, mobile: mobile, image: image, lat: lat, lng: lng, balance: balance, token: token, userType: userType, activation_code: activation_code,promo_code:promo_code,total_purchases:total_purchases, verified: verified, createdOn: createdOn)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.iD, forKey: "iD")
        aCoder.encode(self.address, forKey: "address")
        aCoder.encode(self.lat, forKey: "lat")
        aCoder.encode(self.lng, forKey: "lng")
        
        aCoder.encode(self.full_name, forKey: "full_name")
        aCoder.encode(self.regionID, forKey: "regionID")
        aCoder.encode(self.email, forKey: "email")
        aCoder.encode(self.image, forKey: "image")
        aCoder.encode(self.balance, forKey: "balance")
        aCoder.encode(self.token, forKey: "token")

        aCoder.encode(self.userType, forKey: "userType")
        aCoder.encode(self.activation_code, forKey: "activation_code")
        aCoder.encode(self.verified, forKey: "verified")
        aCoder.encode(self.createdOn, forKey: "createdOn")
        aCoder.encode(self.mobile, forKey: "mobile")
        aCoder.encode(self.promo_code, forKey: "promo_code")
        aCoder.encode(self.total_purchases, forKey: "total_purchases")

        
    }
    
}
