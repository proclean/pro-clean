
import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class OrderData {
	public var iD : Int?
	public var clientID : Int?
	public var client_name : String?
	public var client_mobile : Int?
	public var shift : String?
	public var incomingDelegateID : Int?
	public var outgoingDelegateID : Int?
	public var address : String?
	public var lat : Double?
	public var lng : Double?
	public var totalCost : Double?
	public var discount : Double?
	public var total : Double?
	public var totalItems : Int?
	public var promo_code : String?
	public var status : String?
	public var notes : String?
	public var createdOn : String?
	public var rating : Rating?
	public var client_image : String?
	public var incoming_delegate_name : String?
	public var incoming_delegate_image : String?
	public var incoming_delegate_mobile : String?
	public var outgoing_delegate_name : String?
	public var outgoing_delegate_image : String?
	public var outgoing_delegate_mobile : String?
	public var credit_charges : Int?
	public var cash_charges : Int?
	public var orderDetails : Array<String>?
	public var receipt : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let orderData_list = OrderData.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of OrderData Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [OrderData]
    {
        var models:[OrderData] = []
        for item in array
        {
            models.append(OrderData(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let orderData = OrderData(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: OrderData Instance.
*/
	required public init?(dictionary: NSDictionary) {

		iD = dictionary["ID"] as? Int
		clientID = dictionary["clientID"] as? Int
		client_name = dictionary["client_name"] as? String
		client_mobile = dictionary["client_mobile"] as? Int
		shift = dictionary["shift"] as? String
		incomingDelegateID = dictionary["incomingDelegateID"] as? Int
		outgoingDelegateID = dictionary["outgoingDelegateID"] as? Int
		address = dictionary["address"] as? String
		lat = dictionary["lat"] as? Double
		lng = dictionary["lng"] as? Double
		totalCost = dictionary["totalCost"] as? Double
		discount = dictionary["discount"] as? Double
		total = dictionary["total"] as? Double
		totalItems = dictionary["totalItems"] as? Int
		promo_code = dictionary["promo_code"] as? String
		status = dictionary["status"] as? String
		notes = dictionary["notes"] as? String
		createdOn = dictionary["createdOn"] as? String
		if (dictionary["rating"] != nil) { rating = Rating(dictionary: dictionary["rating"] as! NSDictionary) }
		client_image = dictionary["client_image"] as? String
		incoming_delegate_name = dictionary["incoming_delegate_name"] as? String
		incoming_delegate_image = dictionary["incoming_delegate_image"] as? String
		incoming_delegate_mobile = dictionary["incoming_delegate_mobile"] as? String
		outgoing_delegate_name = dictionary["outgoing_delegate_name"] as? String
		outgoing_delegate_image = dictionary["outgoing_delegate_image"] as? String
		outgoing_delegate_mobile = dictionary["outgoing_delegate_mobile"] as? String
		credit_charges = dictionary["credit_charges"] as? Int
		cash_charges = dictionary["cash_charges"] as? Int
        if (dictionary["orderDetails"] != nil) {
            orderDetails = dictionary["orderDetails"] as? [String]
        }
//        if (dictionary["orderDetails"] != nil) { orderDetails = OrderDetails.modelsFromDictionaryArray(dictionary["orderDetails"] as! NSArray) }
		receipt = dictionary["receipt"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.iD, forKey: "ID")
		dictionary.setValue(self.clientID, forKey: "clientID")
		dictionary.setValue(self.client_name, forKey: "client_name")
		dictionary.setValue(self.client_mobile, forKey: "client_mobile")
		dictionary.setValue(self.shift, forKey: "shift")
		dictionary.setValue(self.incomingDelegateID, forKey: "incomingDelegateID")
		dictionary.setValue(self.outgoingDelegateID, forKey: "outgoingDelegateID")
		dictionary.setValue(self.address, forKey: "address")
		dictionary.setValue(self.lat, forKey: "lat")
		dictionary.setValue(self.lng, forKey: "lng")
		dictionary.setValue(self.totalCost, forKey: "totalCost")
		dictionary.setValue(self.discount, forKey: "discount")
		dictionary.setValue(self.total, forKey: "total")
		dictionary.setValue(self.totalItems, forKey: "totalItems")
		dictionary.setValue(self.promo_code, forKey: "promo_code")
		dictionary.setValue(self.status, forKey: "status")
		dictionary.setValue(self.notes, forKey: "notes")
		dictionary.setValue(self.createdOn, forKey: "createdOn")
		dictionary.setValue(self.rating?.dictionaryRepresentation(), forKey: "rating")
		dictionary.setValue(self.client_image, forKey: "client_image")
		dictionary.setValue(self.incoming_delegate_name, forKey: "incoming_delegate_name")
		dictionary.setValue(self.incoming_delegate_image, forKey: "incoming_delegate_image")
		dictionary.setValue(self.incoming_delegate_mobile, forKey: "incoming_delegate_mobile")
		dictionary.setValue(self.outgoing_delegate_name, forKey: "outgoing_delegate_name")
		dictionary.setValue(self.outgoing_delegate_image, forKey: "outgoing_delegate_image")
		dictionary.setValue(self.outgoing_delegate_mobile, forKey: "outgoing_delegate_mobile")
		dictionary.setValue(self.credit_charges, forKey: "credit_charges")
		dictionary.setValue(self.cash_charges, forKey: "cash_charges")
		dictionary.setValue(self.receipt, forKey: "receipt")

		return dictionary
	}

}
