import Foundation


 class Client {
    public var result : String?
    public var userData : UserData?
    public var msg : String?
    
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let json4Swift_Base_list = Json4Swift_Base.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Json4Swift_Base Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [Client]
    {
        var models:[Client] = []
        for item in array
        {
            models.append(Client(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let json4Swift_Base = Json4Swift_Base(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Json4Swift_Base Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        result = dictionary["result"] as? String
        if (dictionary["userData"] != nil) { userData = UserData(dictionary: dictionary["userData"] as! NSDictionary) }
        
       
        msg = dictionary["msg"] as? String
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.result, forKey: "result")
        dictionary.setValue(self.userData?.dictionaryRepresentation(), forKey: "userData")
        dictionary.setValue(self.msg, forKey: "msg")
        
        return dictionary
    }
    
}
