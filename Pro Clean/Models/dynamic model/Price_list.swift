/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Price_list {
	public var priceIronAndClean : Double?
	public var  priceIron : Double?
	public var priceItem : Double?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let price_list_list = Price_list.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Price_list Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Price_list]
    {
        var models:[Price_list] = []
        for item in array
        {
            models.append(Price_list(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let price_list = Price_list(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Price_list Instance.
*/
	required public init?(dictionary: NSDictionary) {

//        priceIronAndClean = dictionary["3"] as? Double
        priceIronAndClean = Double((dictionary["3"] as! NSString).floatValue * 1000).rounded() / 1000
        
		priceIron = Double((dictionary["2"] as! NSString).floatValue * 1000).rounded() / 1000
		priceItem = Double((dictionary["1"] as! NSString).floatValue * 1000).rounded() / 1000
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.priceIronAndClean, forKey: "3")
		dictionary.setValue(self.priceIron, forKey: "2")
		dictionary.setValue(self.priceItem, forKey: "1")

		return dictionary
	}

}
