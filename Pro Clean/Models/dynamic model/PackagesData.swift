//
//  PackagesData.swift
//  Pro Clean
//
//  Created by Nerneen Mohamed on 5/18/18.
//  Copyright © 2018 Nermeen Mohamed. All rights reserved.
//

import Foundation
public class PackagesData {
    public var iD : Int?
    public var cost : Int?
    public var credit : Int?
    public var image : String?
    public var points : Int?
    public var color : String?
    public var title : String?
    public var description : String?
    public var promo : String?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let packagesData_list = PackagesData.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of PackagesData Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [PackagesData]
    {
        var models:[PackagesData] = []
        for item in array
        {
            models.append(PackagesData(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let packagesData = PackagesData(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: PackagesData Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        iD = dictionary["ID"] as? Int
        cost = dictionary["cost"] as? Int
        credit = dictionary["credit"] as? Int
        image = dictionary["image"] as? String
        points = dictionary["points"] as? Int
        color = dictionary["color"] as? String
        title = dictionary["title"] as? String
        description = dictionary["description"] as? String
        promo = dictionary["promo"] as? String
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.iD, forKey: "ID")
        dictionary.setValue(self.cost, forKey: "cost")
        dictionary.setValue(self.credit, forKey: "credit")
        dictionary.setValue(self.image, forKey: "image")
        dictionary.setValue(self.points, forKey: "points")
        dictionary.setValue(self.color, forKey: "color")
        dictionary.setValue(self.title, forKey: "title")
        dictionary.setValue(self.description, forKey: "description")
        dictionary.setValue(self.promo, forKey: "promo")
        
        return dictionary
    }
    
}
