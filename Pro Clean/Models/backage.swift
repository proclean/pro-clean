//
//  backage.swift
//  Pro Clean
//
//  Created by Maged on 4/18/18.
//  Copyright © 2018 Mego. All rights reserved.
//

import Foundation
struct Package {
    private var  title : String!
    private var  disc : String!
    private var  cost : Int!
    private var  point : Int!
    private var  image : String!

  init  (resDict: Dictionary<String,AnyObject>){
    title = resDict["title"] as? String
    disc = resDict["description"] as? String
    image = resDict["image"] as? String
    point = resDict["point"] as? Int
    cost = resDict["cost"] as? Int
    
}
}
